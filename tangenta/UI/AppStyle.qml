pragma Singleton
import QtQuick 2.8

QtObject {
    id: styleRoot
    // font sizes

    property int regularTextSize: 14
    property int smallTextSize: 10

    property int photoTextHeight: 19
    property int photoTextFont: 8

    property int mainGapWidth: 20
    property int mainGapHeight: 20
    property int extraGapHeight: 5
    property int extraGapWidth: 5
    property int radius: 5
    property color mainColor: "white"
    property color selectedItemColor: "#fdd021"
    property color backColor: "lightgrey"

}
