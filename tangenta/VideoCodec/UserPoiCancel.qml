import QtQuick 2.5

Image {
    id: userPoiCancelRoot
    width: 12
    height: 12
    anchors.top: parent.top
    anchors.right: parent.right
    anchors.margins: 12
    source: "cancel.png"

    signal clicked()

    Behavior on scale {NumberAnimation { duration: 100}}
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        onEntered: {
            parent.scale = 1.1
        }
        onExited:  {
            parent.scale = 1
        }
        onClicked: {
            userPoiCancelRoot.clicked()
        }
    }
}
