import QtQuick 2.0
import "utils.js" as Utils

Page {
    id: root
    title: qsTr("Video Codec")
    property var defaultDecoders: []

    objectName: "videoCodec"
    height: parent.height / 1.5
    width: parent.width / 1.5
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    anchors.verticalCenterOffset: -20

    QtObject {
        id: d
        property Item selectedItem
        property string selectedItemName
        property string detail: qsTr("Takes effect on the next play")
    }

    Column {
        anchors.fill: content
        spacing: Utils.kSpacing
        Text {
            id: detail
            text: d.detail
            color: "white"
            height: contentHeight + 1.6*Utils.kItemHeight
            width: parent.width
            font.pixelSize: 14
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        ListView {
            id: listView
            contentWidth: parent.width - Utils.scaled(20)
            height: Utils.kItemHeight
            anchors {
                //topMargin: Utils.kMargin
                horizontalCenter: parent.horizontalCenter
            }
            onContentWidthChanged: {
                anchors.leftMargin = Math.max(10, (parent.width - contentWidth)/2)
                anchors.rightMargin = anchors.leftMargin
                width = parent.width - 2*anchors.leftMargin
            }
            orientation: ListView.Horizontal
            spacing: Utils.scaled(6)
            //focus: true

            delegate: contentDelegate
            model: codecMode
        }

        ListModel {
            id: codecMode
            //ListElement { name: "Auto"; hardware: true; zcopy: true; description: qsTr("Try hardware decoders first") }
            ListElement { name: "FFmpeg"; hardware: false; zcopy: false; description: "FFmpeg/Libav\n" }
        }

        Component {
            id: contentDelegate
            DelegateItem {
                id: delegateItem
                text: name
                //width: Utils.kItemWidth
                height: Utils.kItemHeight
                color: "#aa000000"
                selectedColor: "#896B3E"

                border.color: "white"
                onClicked: {
                    if (d.selectedItem == delegateItem)
                        return
                    if (d.selectedItem)
                        d.selectedItem.state = "baseState"
                    d.selectedItem = delegateItem
                }
                onStateChanged: {
                    if (state != "selected")
                        return
                    d.detail = description + " " + (hardware ? qsTr("hardware decoding") : qsTr("software decoding"))
                    d.selectedItemName = name
                }
            }
        }
    }

    function setInitialCurrentIndex() {
        for(var i = 0; i < codecMode.count; i++) {
            if(codecMode.get(i).name == "D3D11") {
                return i
            }
            if(codecMode.get(i).name == SettingsManager.videoCodec) {
                return i
            }
        }
        return 0
    }

    Component.onCompleted: {
        if (Qt.platform.os == "windows") {
            defaultDecoders.push("DXVA")
            defaultDecoders.push("D3D11")
            defaultDecoders.push("CUDA")
            codecMode.append({ name: "DXVA", hardware: true, zcopy: true, description: "DirectX Video Acceleration (Windows)\nUse OpenGLES(ANGLE) + D3D" })
            codecMode.append({ name: "D3D11", hardware: true, zcopy: true, description: "D3D11 Video Acceleration\n" })
            codecMode.append({ name: "CUDA", hardware: true, zcopy: true, description: "NVIDIA CUDA (Windows, Linux).\nH264 10bit support."})
        } else if (Qt.platform.os == "winrt" || Qt.platform.os == "winphone") {
            defaultDecoders.push("D3D11")
            codecMode.append({ name: "D3D11", hardware: true, zcopy: true, description: "D3D11 Video Acceleration" })
        } else if (Qt.platform.os == "osx") {
            defaultDecoders.push("VideoToolbox")
            defaultDecoders.push("VDA")
            codecMode.append({ name: "VideoToolbox", hardware: true, zcopy: true, description: "VideoToolbox (OSX)\n" })
            codecMode.append({ name: "VDA", hardware: true, zcopy: true, description: "VDA (OSX)\n" })
        } else if (Qt.platform.os == "ios") {
            defaultDecoders.append("VideoToolbox")
            codecMode.append({ name: "VideoToolbox", hardware: true, zcopy: true, description: "VideoToolbox (iOS)" })
        } else if(Qt.platform.os == "android") {
            defaultDecoders.push("MediaCodec")
            codecMode.append({ name: "MediaCodec", hardware: true, zcopy: false, description: "Android(>=4.1) MediaCodec (H.264)" })
        } else if (Qt.platform.os == "linux") {
            defaultDecoders.push("VAAPI")
            defaultDecoders.push("CUDA")
            codecMode.append({ name: "VAAPI", hardware: true, zcopy: true, description: "VA-API (Linux) " })
            codecMode.append({ name: "CUDA", hardware: true, zcopy: true, description: "NVIDIA CUDA (Windows, Linux)"})
        }
        defaultDecoders.push("FFmpeg")
        listView.currentIndex = setInitialCurrentIndex();
        d.selectedItem = listView.currentItem
        listView.currentItem.state = "selected"
    }

    UserPoiCancel {
        onClicked: {
            console.log(" selectedItemName = " + d.selectedItemName)
            SettingsManager.videoCodec = d.selectedItemName
            root.destroy()
        }
    }
}
