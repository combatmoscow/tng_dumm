import QtQuick 2.0
import "utils.js" as Utils

Rectangle {
    id: root
    layer.enabled: true
    color: "#aa1a2b3a"
    border.color: "#D6D7D8"
    border.width: 1
    //focus: true
    property alias title: title.text
    property alias content: content
    property real titleHeight: title.height + 2*Utils.kMargin
    property real maxHeight: Utils.scaled(300)

    Text {
        id: title
        anchors.top: parent.top
        width: parent.width
        height: Utils.scaled(40)
        color: "white"
        font.pixelSize: Utils.scaled(20)
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        height: 1
        anchors.left: parent.left
        anchors.right: parent.right
        color: "#D6D7D8"
        anchors.top: parent.top
        anchors.topMargin: title.height
    }

    MouseArea { // avoid mouse events propagated to parents
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: false
    }
    Item {
        id: content
        anchors {
            top: title.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        anchors.margins: Utils.kMargin
    }

    Button {
        anchors.top: parent.top
        anchors.right: parent.right
        width: Utils.scaled(20)
        height: Utils.scaled(20)
        bgColor: "transparent"
        bgColorSelected: "transparent"
        //icon: Utils.resurl("theme/default/close.svg")
        onClicked: root.visible = false
    }
}
