#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

#include "SettingsManager.h"
#include "manager.h"
#include "client.h"
#include "serverapi.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationName("Combar Operator");

    qputenv("QT_BEARER_POLL_TIMEOUT", QByteArray::number(-1));
    qputenv("QSG_RENDER_LOOP", "basic");
    QGuiApplication::setAttribute(Qt::AA_UseOpenGLES);
    QQuickView view;

    qmlRegisterType<Server_Api>("tangServerApi", 1, 0, "Server_Api");


    Client client;
    view.rootContext()->setContextProperty("Client", &client);

    view.rootContext()->setContextProperty("Manager", &Manager::instance());
    view.rootContext()->setContextProperty("SettingsManager", &SettingsManager::instance());
    view.rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());

    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    view.setTitle(" Комбат диспетчер ");
    //view.setIcon(QIcon(":/icon_kombat.png"));
    view.setIcon(QIcon("C:/Tangenta_arm/tng_dumm/tangenta/icon_kombat.png"));
    view.setMinimumWidth(1600);
    view.setMinimumHeight(900);
    view.showMinimized();

    return app.exec();
}
