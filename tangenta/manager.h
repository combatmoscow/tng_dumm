#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QVariant>
#include "GpsItem.hpp"

class Manager : public QObject
{
    Q_OBJECT
    explicit Manager(QObject *parent = 0);
public:
    static Manager &instance();
    Q_INVOKABLE QVariant getGpsByTime(int videoIndex, quint64 ms);
    Q_INVOKABLE QVariant getAllTracks();
    Q_INVOKABLE void test();

    void createAllTracks();
    QList<Coord> createFirstTrack();
    QList<Coord> createSecondTrack();
    QList<Coord> createThirdTrack();
    QList<Coord> createFourthTrack();
    QList<Coord> createFifthTrack();

private:
    QList<GpsItem*>   mGpsItems;
};

#endif // MANAGER_H
