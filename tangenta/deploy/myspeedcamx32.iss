; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{88BC4737-69B3-4637-A998-1ED5C9771BCD}
;AppName=Datakam Speedcam Player 7.1
AppName=Tangenta
AppVersion=1.0
AppVerName=Tangenta 1.0
AppPublisher=Combar Company
;AppPublisherURL=datakam.ru
;AppSupportURL=datakam.ru
;AppUpdatesURL=datakam.ru
DefaultDirName={pf}\Tangenta
DefaultGroupName=Tangenta
DisableProgramGroupPage=yes
OutputBaseFilename=Tangenta
;SetupIconFile=C:\DATAKAM\speedcam_player\appicon.ico
;UninstallDisplayIcon=C:\DATAKAM\speedcam_player\appicon.ico
UninstallDisplayIcon={uninstallexe}
Compression=lzma
SolidCompression=yes
;Uninstallable = no

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Tangenta_arm\Deploy\tangenta.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-file-l1-2-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-file-l2-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-localization-l1-2-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-processthreads-l1-1-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-synch-l1-2-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-timezone-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-core-xstate-l2-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-conio-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-convert-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-environment-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-filesystem-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-heap-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-locale-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-math-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-multibyte-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-private-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-process-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-runtime-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-stdio-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-string-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-time-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\api-ms-win-crt-utility-l1-1-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avcodec-57.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avdevice-57.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avfilter-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avformat-57.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avresample-3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\avutil-55.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\concrt140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\D3Dcompiler_47.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\ffmpeg.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\libEGL.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\libGLESV2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\msvcp140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\ntdll.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\opengl32sw.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\libeay32.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\ssleay32.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Tangenta_arm\Deploy\msvcr120.dll"; DestDir: "{app}"; Flags: ignoreversion

Source: "C:\Peter\Deploy\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Location.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Multimedia.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5MultimediaQuick_p.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Positioning.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Qml.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Quick.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "C:\Peter\Deploy\Qt5QuickControls2.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "C:\Peter\Deploy\Qt5QuickTemplates2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5SerialPort.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "C:\Peter\Deploy\Qt5Sql.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Svg.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\QtAV1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\QtAVWidgets1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\swresample-2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\swscale-4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\ucrtbase.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\vcruntime140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\vccorlib140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Peter\Deploy\audio\*"; DestDir: "{app}\audio"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\bearer\*"; DestDir: "{app}\bearer"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\geoservices\*"; DestDir: "{app}\geoservices"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\iconengines\*"; DestDir: "{app}\iconengines"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\imageformats\*"; DestDir: "{app}\imageformats"; Flags: ignoreversion recursesubdirs createallsubdirs
;Source: "C:\Peter\Deploy\Material\*"; DestDir: "{app}\Material"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\mediaservice\*"; DestDir: "{app}\mediaservice"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\platforms\*"; DestDir: "{app}\platforms"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\playlistformats\*"; DestDir: "{app}\playlistformats"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\position\*"; DestDir: "{app}\position"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\private\*"; DestDir: "{app}\private"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\qmltooling\*"; DestDir: "{app}\qmltooling"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\Qt\*"; DestDir: "{app}\Qt"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtAV\*"; DestDir: "{app}\QtAV"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtGraphicalEffects\*"; DestDir: "{app}\QtGraphicalEffects"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtLocation\*"; DestDir: "{app}\QtLocation"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtMultimedia\*"; DestDir: "{app}\QtMultimedia"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtPositioning\*"; DestDir: "{app}\QtPositioning"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtQml\*"; DestDir: "{app}\QtQml"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtQuick\*"; DestDir: "{app}\QtQuick"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\QtQuick.2\*"; DestDir: "{app}\QtQuick.2"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\scenegraph\*"; DestDir: "{app}\scenegraph"; Flags: ignoreversion recursesubdirs createallsubdirs
;Source: "C:\Peter\Deploy\sqldrivers\*"; DestDir: "{app}\sqldrivers"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Peter\Deploy\translations\*"; DestDir: "{app}\translations"; Flags: ignoreversion recursesubdirs createallsubdirs
;Source: "C:\Peter\Deploy\Universal\*"; DestDir: "{app}\Universal"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
;Name: "{commonprograms}\Datakam Speedcam Player"; Filename: "{app}\tangenta.exe"
;Name: "{commondesktop}\Datakam Speedcam Player"; Filename: "{app}\tangenta.exe"; Tasks: desktopicon
Name: "{commonprograms}\Tangenta"; Filename: "{app}\tangenta.exe"
Name: "{commondesktop}\Tangenta"; Filename: "{app}\tangenta.exe"; Tasks: desktopicon
Name: "{group}\Tangenta"; Filename: "{app}\tangenta.exe"
Name: "{group}\Tangenta Uninstall"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\tangenta.exe"; Description: "{cm:LaunchProgram,Tangenta}"; Flags: nowait postinstall skipifsilent

