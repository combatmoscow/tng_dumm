#ifndef SERVERAPI_H
#define SERVERAPI_H

#include <QObject>
#include "server.h"

class Server_Api : public QObject
{
    Q_OBJECT
public:
    explicit Server_Api(QObject *parent = nullptr);

signals:
    void sigConnected();
    void sigDisconnected();
    void sidData(QByteArray data);

public slots:

    bool stopClicked();
    bool startClicked();

    void sltConnectedToServer();
    void sltDisconnectedFromServer();
    void sltSendData(QByteArray data);

private:
    Server *server;
};


#endif // SERVERAPI_H
