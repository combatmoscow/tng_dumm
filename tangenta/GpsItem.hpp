#ifndef GPSITEM_H
#define GPSITEM_H

#include <QString>
#include <QPair>
#include <QList>
#include <QDateTime>
#include <QVariantList>
#include <QDebug>

struct Coord {
    double lon;
    double lat;
    quint64 ts;
};

class GpsItem
{
public:
    GpsItem();
    const QList<Coord> &getCoords()          const { return mCoords;     }
    QVariantList        coordsList()         const { return mCoordsList; }
    QString             baseName()           const { return mBaseName;   }
    void                setBaseName(QString name);
    void                setCoords(QList<Coord> coords);
private:
    QString      mBaseName;
    QVariantList mCoordsList;
    QList<Coord> mCoords;
    quint64 mStartMsTs; // timestamp in ms
};
#endif // GPSITEM_H
