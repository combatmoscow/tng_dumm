#include "SettingsManager.h"

#include <QStandardPaths>
#include <QDir>
#include <QDebug>

SettingsManager::SettingsManager(): QObject(0), mSettings("COMBAT", "OPERATOR")
{
    if (mSettings.value(VIDEO_CODEC).toString().isEmpty()) {
        qDebug() << " SettingsManager videoCodec is empty ";
        setVideoCodec("D3D11");
    } else {
        mVideoCodec    = mSettings.value(VIDEO_CODEC).toString();
        qDebug() << " SettingsManager videoCodec = " << mVideoCodec;
    }
}

SettingsManager &SettingsManager::instance()
{
    static SettingsManager manager;
    return manager;
}

QString SettingsManager::videoCodec()
{
    qDebug() << " videoCodec = " << mVideoCodec;
    return mVideoCodec;
}

void SettingsManager::setVideoCodec(QString videoCodec)
{
    if (mVideoCodec != videoCodec) {
        qDebug() << " SettingsManager setVideoCodec = " << videoCodec;
        mVideoCodec = videoCodec;
        mSettings.setValue(VIDEO_CODEC, mVideoCodec);
        mSettings.sync();
        emit videoCodecChanged(mVideoCodec);
    }
}
