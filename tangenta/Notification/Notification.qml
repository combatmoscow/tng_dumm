import QtQuick 2.5
import "../UI/."

Rectangle {
    id: notificationRoot
    height: 100
    width: 250
    border.width: 1
    border.color: AppStyle.mainColor
    color: AppStyle.mainColor
    radius: AppStyle.radius
    property alias textNotification: mainText.text

    function showNotification() {
        showNot.start()
        closeTimer.start()
    }

    Text {
        id: mainText
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: "NO STREAMING"
        font.pixelSize: AppStyle.regularTextSize
        wrapMode: Text.WordWrap
        color: AppStyle.backColor
    }

    ParallelAnimation {
        id: hideNot
        NumberAnimation { target: notificationRoot; property: "opacity"; to: 0; duration: 1000 }
        NumberAnimation { target: notificationRoot; property: "anchors.topMargin"; to: -350; duration: 1000 }
    }

    Timer {
        id: closeTimer
        triggeredOnStart: false
        repeat: false
        interval: 3000
        onTriggered: {
            hideNot.start()
        }
    }

    ParallelAnimation {
        id: showNot
        NumberAnimation { target: notificationRoot; property: "opacity"; to: 1; duration: 1000 }
        NumberAnimation { target: notificationRoot; property: "anchors.topMargin"; to: 5; duration: 1000 }
    }

}
