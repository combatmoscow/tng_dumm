import QtQuick 2.8
import QtQuick.Window 2.2

import QtLocation 5.6
import QtPositioning 5.6

import "UI/."

Item {
    id: filesRoot
    signal itemClicked(var name, var path)
    property int currentFont: AppStyle.regularTextSize

    Item {
        id: buttonsItem
        anchors.top: parent.top
        anchors.topMargin: AppStyle.mainGapWidth
        anchors.left: parent.left
        anchors.right: parent.right
        height: 20
        //color: "green"

        Item {
            id: nameText
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: AppStyle.mainGapWidth
            anchors.bottom: parent.bottom
            width: parent.width / 4
            //color: "brown"
            Text {
                anchors.left: parent.left
                anchors.verticalCenter: parent.Top
                text: "Имя файла"
                font.pixelSize: filesRoot.currentFont
            }
        }

        Item {
            id: typeText
            anchors.top: parent.top
            anchors.left: nameText.right
            anchors.bottom: parent.bottom
            width: parent.width / 4
            //color: "red"
            Text {
                anchors.left: parent.left
                anchors.leftMargin: AppStyle.mainGapWidth
                anchors.verticalCenter: parent.Top
                text: "Тип"
                font.pixelSize: filesRoot.currentFont
            }
        }
        
        Item {
            id: sizeText
            anchors.top: parent.top
            anchors.left: typeText.right
            anchors.bottom: parent.bottom
            width: parent.width / 4
            //color: "blue"
            Text {
                anchors.left: parent.left
                anchors.verticalCenter: parent.Top
                text: "Размер"
                font.pixelSize: filesRoot.currentFont
            }
        }
        
        Item {
            id: dateText
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: sizeText.right
            anchors.right: parent.right
            width: parent.width / 4
            //color: "white"
            Text {
                anchors.right: parent.right
                anchors.rightMargin: AppStyle.mainGapWidth + 10
                anchors.verticalCenter: parent.Top
                text: "Дата"
                font.pixelSize: filesRoot.currentFont
            }
        }
    }
    
    Rectangle {
        id: separatorItem
        anchors.top: buttonsItem.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: "white"
        height: 1
    }
    
    Item {
        id: mainElement
        anchors.top: separatorItem.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        //color: "green"

        Component.onCompleted: {
            myModel.append({"name": "VIDEO_4141", "path": applicationDirPath + "/VideoForTest/VIDEO_4141.MP4",
                           type: "mp4", size: "1003004", date: "03.02.2011"})
            myModel.append({"name": "VIDEO_4142", "path": applicationDirPath + "/VideoForTest/VIDEO_4141.MP4",
                           type: "mp4", size: "1103004", date: "04.02.2018"})
            myModel.append({"name": "VIDEO_4143", "path": applicationDirPath + "/VideoForTest/VIDEO_4141.MP4",
                           type: "mp4", size: "1203004", date: "05.02.2018"})
            myModel.append({"name": "VIDEO_4144", "path": applicationDirPath + "/VideoForTest/VIDEO_4141.MP4",
                           type: "mp4", size: "1303004", date: "06.02.2018"})
            myModel.append({"name": "VIDEO_4145", "path": applicationDirPath + "/VideoForTest/VIDEO_4141.MP4",
                           type: "mp4", size: "1403004", date: "07.02.2018"})
        }

        ListModel {
            id: myModel
//            ListElement { name: "VIDEO_4141"; type: "mp4"; size: "1003004"; date: "03.02.2018"; path: "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4141.MP4" }
//            ListElement { name: "VIDEO_4142"; type: "mp4"; size: "1103004"; date: "04.02.2018"; path: "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4142.MP4" }
//            ListElement { name: "VIDEO_4143"; type: "mp4"; size: "1203004"; date: "05.02.2018"; path: "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4143.MP4" }
//            ListElement { name: "VIDEO_4144"; type: "mp4"; size: "1303004"; date: "06.02.2018"; path: "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4144.MP4" }
//            ListElement { name: "VIDEO_4145"; type: "mp4"; size: "1403004"; date: "07.02.2018"; path: "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4145.MP4" }
        }
        
        ListView {
            id: view
            anchors.fill: parent
            clip: true
            model: myModel
            delegate: delegateComponent
            highlight: highlightComponent
            //focus: true
            spacing: 1
        }
        
        Component {
            id: highlightComponent
            Rectangle {
                color: AppStyle.selectedItemColor
                width: view.width
            }
        }
        
        Component {
            id: delegateComponent
            Item {
                id: wrapper
                height: 20
                width: ListView.view.width
                
                ListView.onRemove: SequentialAnimation {
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: true }
                    NumberAnimation { target: wrapper; property: "scale"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: false }
                }
                
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        //console.log("adsfasfd")
                        view.currentIndex = index
                        filesRoot.itemClicked(model.name, model.path)
                    }
                }
                
                Item {
                    id: name
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: parent.width / 4
                    anchors.leftMargin: AppStyle.mainGapWidth
                    //color: "brown"
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.name
                        font.pixelSize: filesRoot.currentFont
                    }
                }
                
                Item {
                    id: type
                    anchors.top: parent.top
                    anchors.left: name.right
                    anchors.bottom: parent.bottom
                    width: parent.width / 4
                    //color: "red"
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: AppStyle.mainGapWidth
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.type
                        font.pixelSize: filesRoot.currentFont
                    }
                }
                
                Item {
                    id: size
                    anchors.top: parent.top
                    anchors.left: type.right
                    anchors.bottom: parent.bottom
                    width: parent.width / 4
                    //color: "blue"
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.size
                        font.pixelSize: filesRoot.currentFont
                    }
                }
                
                Item {
                    id: date
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: size.right
                    anchors.right: parent.right
                    width: parent.width / 4
                    //color: "brown"
                    Text {
                        anchors.right: parent.right
                        anchors.rightMargin: AppStyle.mainGapWidth
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.date
                        font.pixelSize: filesRoot.currentFont
                    }
                }
            }
        }
    } 
}
