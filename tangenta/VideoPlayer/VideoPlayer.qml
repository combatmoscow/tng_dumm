import QtQuick 2.5
import QtMultimedia 5.8

import "../Notification/."
import "../UI/."

Rectangle {
    id: playerRoot
    color: AppStyle.backColor

    // properties set
    property alias playbackState:     mediaPlayer.playbackState
    property alias playbackRate:     mediaPlayer.playbackRate
    property alias source:            mediaPlayer.source
    property alias duration:          mediaPlayer.duration
    property alias position:          mediaPlayer.position
    property alias volume:            mediaPlayer.volume
    property alias fastMovingForward: fastPlayingTimer.forward
    property bool  fastMoving:        false
    property int   videoIndex:        -1
    property bool  playing:           mediaPlayer.playbackState == MediaPlayer.PlayingState || fastMoving
    property bool videoPlaying:       playbackState === MediaPlayer.PlayingState || fastMoving
    //property bool _useGps: false

    function showMessage() {
        console.log(" Streaming is not available ")
        streamNot.textNotification = "Server is not available"
        streamNot.open()
    }

    Notification {
        id: streamNot
        anchors.top: parent.top
        anchors.topMargin: -height
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0
        z: 1
        function open() {
            showNotification()
        }
    }

    // functions set
    function play() {
        console.log(" VideoPlayer duration = " + duration + " position = " + position + " source = " + source)
        fastPlayingTimer.running = false
        mediaPlayer.play()
        backButtonItem.visible = true
        //_useGps = Manager.doesVideoHasGps(videoIndex)
    }

    function pause() {
        mediaPlayer.pause()
        fastPlayingTimer.running = false
    }

    function stop() {
        pause()
        mediaPlayer.stop()
        videoIndex = -1
    }
    function nextFrame() {
        pause()
        mediaPlayer.seek(mediaPlayer.position + 500)
    }

    function prevFrame() {
        pause()
        mediaPlayer.seek(mediaPlayer.position - 500)
    }

    function fastRewindForward(koeff) {
        fastMoving = true
        if (mediaPlayer.playbackState === MediaPlayer.PlayingState) {
            mediaPlayer.pause()
        }
        fastPlayingTimer.forward = true
        fastPlayingTimer.koeff = koeff
        fastPlayingTimer.running = true
    }

    function fastRewindBackward(koeff) {
        fastMoving = true
        if (mediaPlayer.playbackState === MediaPlayer.PlayingState) {
            mediaPlayer.pause()
        }
        fastPlayingTimer.forward = false
        fastPlayingTimer.koeff = koeff
        fastPlayingTimer.running = true
    }

    function seek(ms) {
        mediaPlayer.seek(ms)
    }

    signal backButtonClicked()
    signal currentTimeChanged(var position, var gps)
    signal playingFinished()
    property int positionCounter: 0

    Image {
        id: image
        visible: enabled
        anchors.fill: parent
        source: "police.jpg"
        //fillMode: Image.PreserveAspectFit
    }

    VideoOutput {
        id: video
        visible: enabled
        anchors.fill: parent
        source: mediaPlayer
        fillMode: VideoOutput.Stretch
        //focus: true
    }

    MediaPlayer {
        id: mediaPlayer
        onStopped: {
            playerRoot.playingFinished()
            positionCounter = 0
        }
        //playbackRate: 2
        volume: 1
        onPositionChanged: {
            positionCounter++
            if (positionCounter % 3 == 0 && videoIndex != -1) {
                console.log(" VideoPlayer videoIndex = " + videoIndex + " position = " + position)
                var gps = Manager.getGpsByTime(videoIndex, position)
                playerRoot.currentTimeChanged(position, gps)
            }
        }
    }

    Item {
        id: backButtonItem
        visible: false
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 10
        anchors.leftMargin: 5
        rotation: 180
        width: 30
        height: 30

        Rectangle {
            // border
            anchors.fill: parent
            radius: 4
            color: "transparent"
            //border.color: AppStyle.video_control_button_border
            border.color: "white"
            border.width: (backButtonMouseArea.containsMouse) ? 2 : 1
            opacity: {
                if (backButtonMouseArea.containsMouse) {
                    return 1
                }
                return 0
            }
            Behavior on opacity {
                NumberAnimation { duration: 100 }
            }
        }

        Image {
            anchors.centerIn: parent
            height: parent.height - 6
            width: (sourceSize.width / sourceSize.height) * height
            source: "right_arrow.png"
            scale: (backButtonMouseArea.pressed) ? 1.1 : 1
        }
        MouseArea {
            id: backButtonMouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                console.log(" VideoPlayer backButtonItem ")
                backButtonItem.visible = false
                playerRoot.backButtonClicked()
            }
        }
    }

    Timer {
        id: fastPlayingTimer
        property bool forward: true
        property real  koeff: 1
        repeat: true
        interval: 500
        triggeredOnStart: true
        onRunningChanged: {
            fastMoving = running
        }

        onTriggered: {
            var newPosition
            if (forward) {
                newPosition = mediaPlayer.position + koeff * interval
                if (newPosition > mediaPlayer.duration) {
                    newPosition = mediaPlayer.duration
                    running = false
                    mediaPlayer.play()
                }
            } else {
                newPosition = mediaPlayer.position - koeff * interval
                if (newPosition < 0) {
                    newPosition = 0
                    running = false
                    mediaPlayer.pause()
                }
            }
            mediaPlayer.seek(newPosition)
        }
    }

    state: "video"
    states: [
        State {
            name: "video"
            PropertyChanges {  target: image; enabled: false}
            PropertyChanges {  target: video; enabled: true}
        },
        State {
            name: "photo"
            PropertyChanges {  target: image; enabled: true}
            PropertyChanges {  target: video; enabled: false}
            PropertyChanges {  target: mediaPlayer; volume: 0}
        }
    ]
}
