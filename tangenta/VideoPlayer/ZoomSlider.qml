import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: zoomSliderRoot
    width: buttonWidth
    height: buttonWidth * 2 + 5
    property int   buttonWidth: 50
    property int   radius: 2
    property alias buttonZoomInEnable: buttonZoomIn.enabled
    property alias buttonZoomOutEnable: buttonZoomOut.enabled

    signal zoomInClicked()
    signal zoomOutClicked()

    Item{
        id: buttonZoomIn
        anchors.top: parent.top
        width:  parent.width
        height: width
        opacity: (enabled) ? 1 : .75
        Image {
            id: zoomInImage
            anchors.fill: parent
            source: "zoom_in.png"
            scale: (zoomInMouseArea.pressed) ? 1.1 : 1
        }
        MouseArea {
            id: zoomInMouseArea
            anchors.fill: parent
            onClicked: {
                zoomInClicked()
            }
        }
    }

    Item {
        id: buttonZoomOut
        anchors.bottom: parent.bottom
        width: parent.width
        height: width
        opacity: (enabled) ? 1 : .75
        Image {
            id: zoomOutImage
            anchors.fill: parent
            source: "zoom_out.png"
            scale: (zoomOutMouseArea.pressed) ? 1.1 : 1
        }
        MouseArea {
            id: zoomOutMouseArea
            anchors.fill: parent
            onClicked: {
                zoomOutClicked()
            }
        }
    }
}
