import QtQuick 2.8
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls 1.4

import "UI/."
import "Squad/."

Item {
    id: employeesRoot

    property int currentFont: AppStyle.regularTextSize
    property int w_num: 80
    Item {
        id: buttonsItem
        anchors.top: parent.top
        anchors.topMargin: AppStyle.mainGapWidth
        anchors.left: parent.left
        anchors.right: parent.right
        //color: "blue"
        height: 20

        Item {
            id: numberText
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width:  w_num
            //color: "brown"
            Text {
                anchors.left: parent.left
                anchors.leftMargin: AppStyle.mainGapWidth
                anchors.verticalCenter: parent.Top
                text: "ID"
                font.pixelSize: employeesRoot.currentFont
            }
        }
        
        Item {
            id: nameText
            anchors.top: parent.top
            anchors.left: numberText.right
            anchors.bottom: parent.bottom
            width: (parent.width- w_num) / 5
            //color: "red"
            Text {
                anchors.left: parent.left
                anchors.verticalCenter: parent.Top
                text: "Имя"
                font.pixelSize: employeesRoot.currentFont
            }
        }
        
        Item {
            id: surnameText
            anchors.top: parent.top
            anchors.left: nameText.right
            anchors.bottom: parent.bottom
            width: (parent.width- w_num) / 5
            //color: "blue"
            Text {
                anchors.left: parent.left
                anchors.verticalCenter: parent.Top
                text: "Фамилия"
                font.pixelSize: employeesRoot.currentFont
            }
        }
        
        Item {
            id: patriotText
            anchors.top: parent.top
            anchors.left: surnameText.right
            anchors.bottom: parent.bottom
            width: (parent.width- w_num) / 4
            //color: "brown"
            Text {
                anchors.left: parent.left
                anchors.verticalCenter: parent.Top
                text: "Отчество"
                font.pixelSize: employeesRoot.currentFont
            }
        }
        
        Item {
            id: tokenText
            anchors.top: parent.top
            anchors.left: patriotText.right
            anchors.bottom: parent.bottom
            width: (parent.width- w_num) / 5
            //color: "yellow"
            Text {
                anchors.left: parent.left
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.Top
                text: "Жетон"
                font.pixelSize: employeesRoot.currentFont
            }
        }

        Item {
            id: mapText
            anchors.top: parent.top
            anchors.left: tokenText.right
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            //color: "green"
            Text {
                anchors.right: parent.right
                anchors.rightMargin: AppStyle.mainGapWidth + 5
                anchors.verticalCenter: parent.Top
                text: "Карта"
                font.pixelSize: employeesRoot.currentFont
            }
        }
    }
    
    Rectangle {
        id: separatorItem
        anchors.top: buttonsItem.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: "white"
        height: 1
    }
    
    Item {
        anchors.top: separatorItem.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        //color: "green"

        ListView {
            id: view
            anchors.fill: parent
            clip: true
            model: Squads.getCurrentModel()
            delegate: delegateComponent
            highlight: highlightComponent
            //focus: true
            spacing: 1
        }
        
        Component {
            id: highlightComponent
            Rectangle {
                color: AppStyle.selectedItemColor
                width: view.width
            }
        }
        
        Component {
            id: delegateComponent
            Item {
                id: wrapper
                height: 20
                width: ListView.view.width

                ListView.onRemove: SequentialAnimation {
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: true }
                    NumberAnimation { target: wrapper; property: "scale"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: false }
                }
                
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        view.currentIndex = index
                        Squads.currentIndex = index
                    }
                }
                
                Item {
                    id: number
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width:  w_num
                    //color: "brown"
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: AppStyle.mainGapWidth
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.number
                        font.pixelSize: employeesRoot.currentFont
                    }
                }
                
                Item {
                    id: name
                    anchors.top: parent.top
                    anchors.left: number.right
                    anchors.bottom: parent.bottom
                    width: (parent.width- w_num) / 5
                    //color: "red"
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.name
                        font.pixelSize: employeesRoot.currentFont
                    }
                }
                
                Item {
                    id: surname
                    anchors.top: parent.top
                    anchors.left: name.right
                    anchors.bottom: parent.bottom
                    width: (parent.width- w_num) / 5
                    //color: "blue"
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.surname
                        font.pixelSize: employeesRoot.currentFont
                    }
                }
                
                Item {
                    id: patriot
                    anchors.top: parent.top
                    anchors.left: surname.right
                    anchors.bottom: parent.bottom
                    width: (parent.width- w_num) / 4
                    //color: "yellow"
                    
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text: model.patriot
                        font.pixelSize: employeesRoot.currentFont
                    }
                }
                
                Item {
                    id: token
                    anchors.top: parent.top
                    anchors.left: patriot.right
                    anchors.bottom: parent.bottom
                    //anchors.right: parent.right
                    width: (parent.width- w_num) / 5
                    //color: "brown"
                    Text {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        text:model.token
                        font.pixelSize: employeesRoot.currentFont
                    }
                }

                Item {
                    id: map
                    anchors.top: parent.top
                    anchors.left: token.right
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    //color: "blue"
                    CheckBox {
                        anchors.right: parent.right
                        anchors.rightMargin: AppStyle.mainGapWidth
                        anchors.verticalCenter: parent.verticalCenter
                        checked: model.isOnMap
                        onCheckedChanged: {
                            if(checked) {
                                model.isOnMap = true
                            } else {
                                model.isOnMap = false
                            }
                        }
                    }
                }
            }
        }
    }
}
