#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include <QDataStream>

QT_BEGIN_NAMESPACE
class QTcpSocket;
class QNetworkSession;
QT_END_NAMESPACE

class Client : public QObject
{
    Q_OBJECT
public:
    enum ID {
        FIRST = 0,
        SECOND,
        THIRD
    };
    explicit Client(QObject *parent = Q_NULLPTR);
    Q_INVOKABLE void connectToDefiniteHost(int id, const QString &ip, int port);
signals:
    void geoUpdated(QVariantList coords, int index);
private slots:
    void readGeo(Client::ID id);
    void readGeoFirst();
    void readGeoSecond();
    void readGeoThird();
    void displayError(QAbstractSocket::SocketError socketError);
private:
    QTcpSocket *tcpSocket_first;
    QTcpSocket *tcpSocket_second;
    QTcpSocket *tcpSocket_third;
    QList<QTcpSocket *> listSockets;
//    QList<int> listPorts;
//    QList<QString> listAdresses;
    QDataStream in;
};
#endif
