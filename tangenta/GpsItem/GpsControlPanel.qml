import QtQuick 2.5
import "../UI/."

Item {
    id: gpsRootControlPanel

    signal openStream(string ip, string port);

    Column {
        id: buttons
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width / 4
        height: parent.height
        anchors.bottom: parent.bottom
        spacing: 2
        GpsButton {
            onOpenPort: { Client.connectToDefiniteHost(0, gpsIpFirst.text, parseInt(gpsPortFirst.text)) }
        }
        GpsButton {
            onOpenPort: { Client.connectToDefiniteHost(1, gpsIpSecond.text, parseInt(gpsPortSecond.text)) }
        }
        GpsButton {
            onOpenPort: { Client.connectToDefiniteHost(2, gpsIpThird.text, parseInt(gpsPortThird.text)) }
        }
        GpsButton {
            onOpenPort: { openStream(streamIp.text, streamPort.text) }
        }
    }
    
    Column {
        id: names
        anchors.top: parent.top
        anchors.left: buttons.right
        width: parent.width / 4
        height: parent.height
        spacing: 2
        GpsText {
            text: qsTr("Сотрудник 1")
        }
        GpsText {
            text: qsTr("Сотрудник 2")
        }
        GpsText {
            text: qsTr("Сотрудник 3")
        }
        GpsText {
            text: qsTr("Стрим")
        }
    }
    
    Column {
        id: ips
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: names.right
        spacing: 2
        width: (parent.width * 2 / 4) * 3 / 4
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsIpFirst
                text: "192.168.1.40"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsIpSecond
                text: "192.168.1.40"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsIpThird
                text: "192.168.1.40"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: streamIp
                text: "192.168.1.47"
            }
        }
    }
    
    Column {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: ips.right
        anchors.right: parent.right
        spacing: 2
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsPortFirst
                text: "5055"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsPortSecond
                text: "5056"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: gpsPortThird
                text: "5057"
            }
        }
        Item {
            height: AppStyle.photoTextHeight
            width: parent.width
            GpsTextField {
                id: streamPort
                text: "5554"
            }
        }
    }

    state: "hide"
    states: [
        State {
            name: "show"
            PropertyChanges { target: gpsRootControlPanel; opacity: 0.8; anchors.rightMargin: 0 }
        },
        State {
            name: "hide"
            PropertyChanges { target: gpsRootControlPanel; opacity: 0; anchors.rightMargin: -gpsRootControlPanel.width }
        }
    ]
    transitions: [
        Transition {
            from: "*"; to: "*"
            PropertyAnimation {
                properties: "opacity,anchors.rightMargin"
                easing.type: Easing.OutQuart
                duration: 1000
            }
        }
    ]


function _startStreem()
{
   openStream(streamIp.text, streamPort.text)
     console.log(" _startStream")
}



}
