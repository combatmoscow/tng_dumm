import QtQuick 2.5
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.4
import "../UI/."

TextField{
    anchors.fill: parent
    verticalAlignment: Qt.AlignVCenter
    horizontalAlignment: Qt.AlignLeft
    font.pixelSize: AppStyle.smallTextSize
    font.family: "helvetica"
    style: TextFieldStyle {
        textColor: "black"
        background: Rectangle {
            radius: 5
            border.color: "#606060"
            border.width: 1
        }
    }
}
