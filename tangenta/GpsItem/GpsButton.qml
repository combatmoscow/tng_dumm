import QtQuick 2.5
import "../UI/."

Rectangle {
    id: gpsRoot
    width: parent.width
    height: AppStyle.photoTextHeight
    color: AppStyle.backColor
    radius: AppStyle.radius
    border.color: "white"
    border.width: 1

    signal openPort()

    Text {
        text: qsTr("open")
        anchors.centerIn: parent
        font.pixelSize: AppStyle.smallTextSize
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        onClicked: {
            gpsRoot.openPort()
        }
    }
}
