import QtQuick 2.5
import "../UI/."

Text {
    width: parent.width
    height: AppStyle.photoTextHeight
    fontSizeMode: Text.HorizontalFit
    font.pixelSize: AppStyle.smallTextSize
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
}
