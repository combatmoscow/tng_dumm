import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtMultimedia 5.5

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../VideoPlayer"

Rectangle {
    id: controlPanelRoot
    height: mainItem.height + timeSlider.height + 5
    //color: AppStyle.video_control_bg_color
    color: "#1A1A1A"
    property int miniminWidth: 50 * 9
    property int buttonWidth: 45
    property int buttonHeight: 45

    property var videoPlayer: undefined
    property bool videoPlaying: (videoPlayer !== undefined && videoPlayer.playing)

    signal settingsClicked(var checked)
    signal sliderValueChanged()

    onVideoPlayingChanged: {
        console.log("Video playing: " + videoPlaying)
        if (!videoPlaying) {
            resetFastForward()
            resetFastBackward()
        }
    }

    TimeSlider {
        id: timeSlider
        width: parent.width
        position: videoPlayer.position
        duration: (videoPlayer) ? videoPlayer.duration : 0
        anchors.top: parent.top
        anchors.topMargin: 5
        onValueChanged: {
            sliderValueChanged()
            videoPlayer.seek(value)
        }
    }

    function resetFastBackward() {
        buttonFastBackward.checked = false
        buttonFastBackward.currentKoeff = 1
    }

    function resetFastForward() {
        buttonFastForward.checked = false
        buttonFastForward.currentKoeff = 1
    }

    function fastBackward(currentKoeff) {
        resetFastForward()
        if (currentKoeff == 1) {
            buttonFastBackward.checked = true
            videoPlayer.fastRewindBackward(2)
            buttonFastBackward.currentKoeff = 2
        } else if (currentKoeff == 2) {
            buttonFastBackward.checked = true
            videoPlayer.fastRewindBackward(4)
            buttonFastBackward.currentKoeff = 4
        } else if (currentKoeff == 4) {
            buttonFastBackward.checked = false
            videoPlayer.pause()
            buttonFastBackward.currentKoeff = 1
        }
    }

    function fastForward(currentKoeff) {
        //console.log(" ControlPanel fastForward videoPlaying " + videoPlaying + " videoIndex = " + videoPlayer.videoIndex)
        resetFastBackward()
        if (currentKoeff == 1) {
            buttonFastForward.checked = true
            videoPlayer.fastRewindForward(2)
            buttonFastForward.currentKoeff = 2
        } else if (currentKoeff == 2) {
            buttonFastForward.checked = true
            videoPlayer.fastRewindForward(4)
            buttonFastForward.currentKoeff = 4
        } else if (currentKoeff == 4) {
            buttonFastForward.checked = false
            videoPlayer.pause()
            buttonFastForward.currentKoeff = 1
        }
    }

    function playPause() {
        if (videoPlaying) {
            videoPlayer.pause()
        } else {
            videoPlayer.play()
        }
    }

    function prevFrame() {
        videoPlayer.prevFrame()
    }

    function nextFrame() {
        videoPlayer.nextFrame()
    }


    ControlButton {
        id: buttonSettings
        width: buttonWidth
        height: mainItem.height
        anchors.left: parent.left
        anchors.verticalCenter: mainItem.verticalCenter
        imageSource: "gear.png"
        //checkable: true
        checked: false
        onClicked: {
            checked = !checked
            controlPanelRoot.settingsClicked(checked)
        }
    }
    Item {
        id: mainItem
        height: buttonHeight
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        //width: controlsRow.width + shareMenu.width
        width: controlsRow.width
        clip: true
        Row {
            id: controlsRow
            width: children.length * buttonWidth
            height: parent.height
            anchors.left: parent.left
            ControlButton {
                id: buttonFastBackward
                property int currentKoeff: 1
                width: buttonWidth
                height: parent.height
                //checkable: true
                imageSource: {
                    switch (currentKoeff) {
                    default:
                    case 1:
                        return "backward.png"
                    case 2:
                        return "backward2x.png"
                    case 4:
                        return "backward4x.png"
                    }
                }
            }
            ControlButton {
                id: buttonPrevFrame
                width: buttonWidth
                height: parent.height
                imageSource: "prev_frame.png"
            }
            ControlButton {
                id: buttonPlayPause
                width: buttonWidth
                height: parent.height
                imageSource: (videoPlaying)
                             ? "pause.png"
                             : "play.png"
            }
            ControlButton {
                id: buttonNextFrame
                width: buttonWidth
                height: parent.height
                imageSource: "next_frame.png"
            }
            ControlButton {
                id: buttonFastForward
                property int currentKoeff: 1
                width: buttonWidth
                height: parent.height
                //checkable: true
                imageSource: {
                    switch (currentKoeff) {
                    default:
                    case 1:
                        return "forward.png"
                    case 2:
                        return "forward2x.png"
                    case 4:
                        return "forward4x.png"
                    }
                }
            }
        }
    }

    property real volumeValue: 1.0
    signal volumeChanged(var volume)
    Slider {
        id: volumeSlider
        height: 15
        width: 40
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: mainItem.verticalCenter

        maximumValue: 1
        minimumValue: 0
        value: volumeValue

        onValueChanged: {
            console.log(" ControlPanel volumeSlider valueChanged = " + value)
            volumeChanged(value)
        }

        style: SliderStyle {
            handle: Rectangle {
                height: volumeSlider.height - 4
                width: height
                radius: width / 2
                //color: AppStyle.volume_slider_round_color
                color: "#fff"

            }

            groove: Rectangle {
                implicitHeight: volumeSlider.height / 4
                implicitWidth: 150
                radius: height / 2
                //border.color: AppStyle.volume_slider_band_border_color
                border.color: "#C54F34"
                //color: AppStyle.volume_slider_band_color
                color: "#222"
                Rectangle {
                    height: parent.height
                    width: styleData.handlePosition
                    implicitHeight: 6
                    implicitWidth: 100
                    radius: height / 2
                    //color: AppStyle.volume_slider_band_border_color
                    color: "#C54F34"
                }
            }
        }
    }

    property int videoIndex: videoPlayer.videoIndex

    state: "hide"
    states: [
        State {
            name: "show"
            PropertyChanges { target: controlPanelRoot; opacity: 0.7; anchors.rightMargin: 0; }
            PropertyChanges {  target: controlsRow; enabled: true}
            PropertyChanges {  target: buttonFastBackward; onClicked: {fastBackward(buttonFastBackward.currentKoeff)}}
            PropertyChanges {  target: buttonPrevFrame; onClicked: { prevFrame() }}
            PropertyChanges {  target: buttonPlayPause; onClicked: { playPause() }}
            PropertyChanges {  target: buttonNextFrame; onClicked: { nextFrame() }}
            PropertyChanges {  target: buttonFastForward; onClicked: {fastForward(buttonFastForward.currentKoeff)}}
            StateChangeScript { script: timeSlider.updatePositionText(); }
        },
        State {
            name: "hide"
            PropertyChanges { target: controlPanelRoot; opacity: 0; anchors.rightMargin: -controlPanelRoot.width }
            PropertyChanges {  target: buttonFastBackward; onClicked: {}}
            PropertyChanges {  target: buttonPrevFrame; onClicked: {}}
            PropertyChanges {  target: buttonPlayPause; onClicked: {}}
            PropertyChanges {  target: buttonNextFrame; onClicked: {}}
            PropertyChanges {  target: buttonFastForward; onClicked: {}}
        }
    ]
    transitions: [
        Transition {
            from: "*"; to: "*"
            PropertyAnimation {
                properties: "opacity,anchors.rightMargin"
                easing.type: Easing.OutQuart
                duration: 1000
            }
        }
    ]
}
