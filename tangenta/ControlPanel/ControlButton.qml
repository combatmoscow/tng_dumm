import QtQuick 2.5

Rectangle {
    id: controlButtonRoot
    color: "transparent"
    //property bool  checkable:   false
    property bool  checked:     false
    property alias imageSource: image.source
    property int margin: 8
    signal clicked()

    Rectangle {
        // background
        anchors.fill: parent
        anchors.margins: 5
        radius: 4
        color: "black"
        opacity: (controlButtonMouseArea.pressed) ? .1 : 0
        Behavior on opacity {
            NumberAnimation { duration: 100 }
        }
    }

    Rectangle {
        // border
        anchors.fill: parent
        anchors.margins: 5
        radius: 4
        color: "transparent"
        //border.color: AppStyle.video_control_button_border
        border.color: "white"
        border.width: (controlButtonMouseArea.containsMouse) ? 2 : 1
        opacity: {
            if (controlButtonMouseArea.containsMouse || checked) {
                return 1
            }
            return 0
        }
        Behavior on opacity {
            NumberAnimation { duration: 100 }
        }
    }

    Image {
        id: image
        anchors.fill: parent
        anchors.margins: margin
        //source: AppStyle.ui_gear_image
        source: "gear.png"
        smooth: true
        //opacity: (controlButtonRoot.enabled) ? 1 : .5
    }

    MouseArea {
        id: controlButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            controlButtonRoot.clicked()
        }
    }


    state: "enableCondition"
    states: [
        State {
            name: "enableCondition"
            PropertyChanges {  target: image; opacity: 1}
            PropertyChanges {  target: controlButtonRoot; enabled: true}
            PropertyChanges {  target: controlButtonMouseArea; hoverEnabled: true}
        },
        State {
            name: "disableCondition"
            PropertyChanges {  target: image; opacity: 1}
            PropertyChanges {  target: controlButtonRoot; enabled: false}
            PropertyChanges {  target: controlButtonMouseArea; hoverEnabled: true}
        },
        State {
            name: "disableFoggyCondition"
            PropertyChanges {  target: image; opacity: .5}
            PropertyChanges {  target: controlButtonRoot; enabled: false}
            PropertyChanges {  target: controlButtonMouseArea; hoverEnabled: true}
        }
    ]
}
