import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "Time.js" as F

Item {
    id: timeSliderRoot
    height: 10
    property int regularTextSize: 12

    property int position: 0
    property int duration: 0
    onPositionChanged: {
        if (!slider.pressed) {
            slider.value = position
        }
    }

    signal valueChanged(var value)

    function resetPositionText() {
        positionText.text = F.msToString(0)
    }
    function updatePositionText() {
        positionText.text = Qt.binding(function() { return F.msToString(position) })
    }

    Item {
        id: positionTextItem
        height: parent.height
        width: parent.width / 10
        anchors.left: parent.left
        Text {
            id: positionText
            font.pixelSize: regularTextSize
            anchors.centerIn: parent
            text: F.msToString(position)
            color: "white"
        }
    }

    Slider {
        id: slider
        width: parent.width - durationTextItem.width -
               positionTextItem.width - 10
        height: parent.height
        anchors.centerIn: parent
        minimumValue: 0
        maximumValue: duration
        updateValueWhileDragging: false
        onPressedChanged: {
            if (!pressed) {
                timeSliderRoot.valueChanged(value)
            }
        }
        style: SliderStyle {
            id: sliderStyle
            groove: Rectangle {
                id: grooveRect
                height: 3
                color: "white"
                Rectangle {
                    // filled part
                    id: filledPart
                    anchors.left: parent.left
                    height: parent.height
                    color: "#DDDDDD"
                    width: styleData.handlePosition
                }
                Rectangle {
//                    MouseArea {
//                        anchors.fill: parent
//                        onClicked: {
//                            handleRect.height = handleRect.height + 1
//                            console.log(" handleRect.height = " + handleRect.height)
//                        }
//                    }

                    id: currentValueText
                    height: 20
                    width: height * 3
                    anchors.horizontalCenter: filledPart.right
                    anchors.bottom: parent.top
                    anchors.bottomMargin: (timeSliderRoot.height - grooveRect.height) / 2 + 5
                    color: "transparent"
                    visible: control.pressed
                    Rectangle {
                        // bg
                        anchors.fill: parent
                        color: "black"
                        opacity: .5
                    }
                    Text {
                        font.pixelSize: parent.height * .6
                        anchors.centerIn: parent
                        //text: F.msToString((styleData.handlePosition - 8) / (control.width - 16) * duration)
                        text: F.msToString((styleData.handlePosition - 8) / (control.width - 16) * duration)
                        color: "white"
                    }
                }
            }
            handle: Rectangle {
                id: handleRect
                anchors.centerIn: parent
                height: slider.height + 1
                width: height
                radius: width / 2
                color: "#e30413"
            }
        }
    }

    Item {
        id: durationTextItem
        height: parent.height
        width: parent.width / 10
        anchors.right: parent.right
        Text {
            id: durationText
            font.pixelSize: regularTextSize
            anchors.centerIn: parent
            //text: F.msToString(duration)
            text: F.msToString(duration)
            color: "white"
        }
    }
}
