#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QSettings>
#include <QFileInfo>

#define VIDEO_CODEC    QString("VIDEO_CODEC")

class SettingsManager : public QObject
{
    Q_OBJECT
    SettingsManager();
    Q_PROPERTY(QString videoCodec READ videoCodec WRITE setVideoCodec NOTIFY videoCodecChanged)
public:
    static SettingsManager &instance();
    QString videoCodec();
    void setVideoCodec(QString videoCodec);
signals:
    void videoCodecChanged(QString videoCodec);
private:
    QSettings mSettings;
    QString mVideoCodec;
};

#endif // SETTINGSMANAGER_H
