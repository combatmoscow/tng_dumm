import QtQuick 2.8
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtGraphicalEffects 1.0
import tangServerApi 1.0


import "photosItems/."
import "ControlPanel/."
import "VideoPlayer/."
import "UI/."
import "MapItem/."
import "GpsItem/."


Rectangle {
    id: mainWindow
    visible: true
    color: AppStyle.mainColor

//    Server_Api
//    {
//       id:server_api

//    }




    Item {
        id: mainItem
        anchors.fill: parent
        anchors.margins: 20
        //color: "red"
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onMouseXChanged: {
                if (mouseX <= mainItem.width - tLRect.width && controlPanel.state === "show") {
                    controlPanel.state = "hide"
                }
            }
            onMouseYChanged: {
                if (mouseY >= tLRect.height && controlPanel.state === "show") {
                    controlPanel.state = "hide"
                }
            }
        }

        Rectangle {
            anchors.fill: parent
            color: AppStyle.mainColor

            Item {
                id: tLRect
                anchors.top: parent.top
                anchors.left: parent.left
                height: (parent.height - AppStyle.mainGapHeight) / 2
                width: (parent.width - AppStyle.mainGapWidth) / 2
                //color: "green"

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.ArrowCursor

                    onMouseXChanged: {
                        if (mouseX < mapControlPanel.width) {
                            mapControlPanel.state = "show"
                        } else {
                            mapControlPanel.state = "hide"
                        }
                    }
                    onMouseYChanged: {
                        if(mouseY < gpsControlPanel.height) {
                            gpsControlPanel.state = "show"
                        } else {
                            gpsControlPanel.state = "hide"
                        }
                    }
                }

                GpsControlPanel {
                    id: gpsControlPanel
                    anchors.top: parent.top
                    anchors.right: parent.right
                    width: parent.width / 2.2
                    height: parent.height / 4
                    z: 1

                    Timer {
                        id: httpRequestTimeout
                        property bool timeoutExpired: false
                        interval: 10000; running: false; repeat: false
                        onTriggered: {
                            httpRequestTimeout.timeoutExpired = true
                            httpRequestTimeout.stop()
                            console.log(" server is not available ");
                            videoPlayer.showMessage()
                        }
                    }



                    onOpenStream: {
                        console.log(" onOpenStream ")
                        var xhr = new XMLHttpRequest();
                        xhr.onreadystatechange = httpRequestTimeout.timeoutExpired
                                  ? function() {}
                                  : function() {
                            httpRequestTimeout.stop()
                            if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                                print('HEADERS_RECEIVED');
                            } else if(xhr.readyState === XMLHttpRequest.DONE) {
                                print('DONE');
                                if(xhr.responseText === "") {
                                    console.log(" server is not available ");
                                    videoPlayer.showMessage()
                                } else {
                                    console.log("hello = " + xhr.responseText);
                                    videoPlayer.openStream(ip, port)
                                }
                            }
                        }
                        //xhr.open('GET', "http://192.168.1.47:8080/playlist.m3u", true);
                        xhr.open('GET', "http://" + ip + ":8080/playlist.m3u")
                        httpRequestTimeout.timeoutExpired = false
                        httpRequestTimeout.start()
                        xhr.send()
                    }
                }

                MapControlPanel {
                    id: mapControlPanel
                    width: 60
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    color: AppStyle.backColor
                    z: 1
                    radius: AppStyle.radius
                }

                Item {
                    id: map
                    anchors.fill: parent
                    MapItem {
                        id: mapItem
                        anchors.fill: parent
                    }
                }
            }

            Item {
                anchors.top: parent.top
                anchors.right: parent.right
                height: (parent.height - AppStyle.mainGapHeight) / 2
                width: (parent.width - AppStyle.mainGapWidth) / 2

                Item {
                    id: player
                    anchors.fill: parent

                    layer.enabled: true
                    layer.effect: OpacityMask {
                        maskSource: Item {
                            width: player.width
                            height: player.height
                            Rectangle {
                                anchors.centerIn: parent
                                width: parent.width
                                height: parent.height
                                radius: AppStyle.radius
                            }
                        }
                    }

                    VideoPlayer {
                        id: videoPlayer
                        anchors.fill: parent
                        enabled: visible
                        function openStream(ip, port) {
                            videoPlayer.videoIndex = -1
                            videoPlayer.source = Qt.resolvedUrl("rtsp://" + ip + ":" + port + "/back")
                            videoPlayer.play()
                            //videoPlayer.source = "C:/Tangenta_arm/tng_dumm/tangenta/VideoTest/VIDEO_4145.MP4"
                            //videoPlayer.noStreamingTimerStart()
                        }

                        function openFile(name, path) {
                            console.log(" onItemClicked name = " + name + " path = " + path)
                            videoPlayer.videoIndex = Number(name.slice(-2)) // get 42 from VIDEO_4142.MP4
                            videoPlayer.source = path
                            console.log(" main.qml videoPlayer.videoIndex = " + videoPlayer.videoIndex)
                            videoPlayer.play()
                            mapItem.setCurrentTrack(Number(name.slice(-2)))
                        }

                        onVideoPlayingChanged: {
                            console.log("Video playing: " + videoPlaying)
                            if (!videoPlaying) {
                                mapItem.hideCarMark()
                            } else {
                                mapItem.showCarMark()
                            }
                        }
                        onCurrentTimeChanged: mapItem.updateCarPos(gps)
                        onBackButtonClicked: {
                            videoPlayer.stop()
                            mapItem.hideCarMark()
                        }
                        onPlayingFinished: {
                            videoPlayer.seek(0)
                            mapItem.videoPlayFinished()
                        }
                    }

                    function showVideoCodecItem() {
                        for(var i = 0; i < player.children.length; i++) {
                            console.log(" win.children[i].objectNam = " + player.children[i].objectName)
                            if(player.children[i].objectName === "videoCodec") {
                                return
                            }
                        }
                        var component = Qt.createComponent("VideoCodec/VideoCodec.qml")
                        if (component.status === Component.Ready || component.status === Component.Error) {
                            if (component.status === Component.Ready) {
                                //var videoCodecItem = component.createObject(player, { "anchors.bottomMargin": controlPanel.height + 10});
                                var videoCodecItem = component.createObject(player);
                                if (videoCodecItem === null) {
                                    console.log("Error creating VideoCodec.qml ");
                                }
                            } else if (component.status === Component.Error) {
                                console.log("Error loading component VideoCodec:", component.errorString());
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        anchors.topMargin: 30
                        hoverEnabled: true
                        cursorShape: Qt.ArrowCursor
                        onMouseYChanged: {
                            if (videoPlayer.videoIndex != -1 && parent.height - mouseY < controlPanel.height +
                                    anchors.topMargin) {
                                controlPanel.state = "show"
                            } else {
                                controlPanel.state = "hide"
                            }
                        }
                    }

                    ControlPanel {
                        id: controlPanel
                        width: parent.width
                        anchors.bottom: parent.bottom
                        videoPlayer: videoPlayer

                        onSettingsClicked: {
                            if(Qt.platform.os == "windows") {
                                player.showVideoCodecItem();
                            }
                        }
                        onVolumeChanged: {
                            videoPlayer.volume = volume
                        }
                        onSliderValueChanged: {
                            mapItem.resetMoveAnimation()
                        }
                    }
                }
            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                height: (parent.height - AppStyle.mainGapHeight) / 2
                width: (parent.width - AppStyle.mainGapWidth) / 2
                color: AppStyle.mainColor

                Rectangle {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: (parent.width - AppStyle.mainGapWidth) / 2
                    color: AppStyle.backColor
                    radius: AppStyle.radius

                    Item {
                        id: employees
                        anchors.fill: parent
                        //color: "red"
                        EmployeesItem {
                            anchors.fill: parent
                        }
                    }
                }

                Rectangle {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    width: (parent.width - AppStyle.mainGapWidth) / 2
                    color: AppStyle.backColor
                    radius: AppStyle.radius

                    Item {
                        id: photos
                        anchors.fill: parent
                        anchors.leftMargin: AppStyle.mainGapWidth
                        anchors.rightMargin: AppStyle.mainGapWidth
                        anchors.topMargin: AppStyle.mainGapWidth
                        //color: "green"
                        PhotosItem {
                            id: photosItem                            
                            anchors.fill: parent

                            onStartStream:
                            {
                              console.log(" onStartStream")
                               gpsControlPanel._startStreem()
                            }

                        }
                    }
                }
            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                height: (parent.height - AppStyle.mainGapHeight) / 2
                width: (parent.width - AppStyle.mainGapWidth) / 2
                color: AppStyle.mainColor

                Rectangle {
                    id: gridPhotos
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: (parent.width - AppStyle.mainGapWidth) / 2

                    GridView {
                        id: myGrid
                        anchors.fill: parent
                        cellWidth: parent.width / 2; cellHeight: parent.height / 2
                        focus: true

                        model: ListModel {
                            ListElement { photo: "combatVideoSystem2.png";
                                modelnumber: "SRK 2115"; serialnumber: "XGH57TRF"; status: "В работе";
                                tAnch: true;  bAnch: false; lAnch: true;  rAnch: false;  }
                            ListElement { photo: "combatAutoStation2.png";
                                modelnumber: "Kombat 2000"; serialnumber: "XGH58TRF"; status: "В работе";
                                tAnch: true;  bAnch: false; lAnch: false; rAnch: true;   }
                            ListElement { photo: "combatRadioStation2.png";
                                modelnumber: "Моторола DP4801"; serialnumber: "XGH59TRF"; status: "В работе";
                                tAnch: false; bAnch: true;  lAnch: true;  rAnch: false;  }
                            ListElement { photo: "AndroidStation2.png";
                                modelnumber: "Huawei Z150"; serialnumber: "XGH60TRF"; status: "В работе";
                                tAnch: false; bAnch: true;  lAnch: false; rAnch: true;  }
                        }
                        delegate: Component {
                            Rectangle {
                                width: myGrid.cellWidth
                                height: myGrid.cellHeight
                                color: AppStyle.mainColor
                                property bool isCurrentItem: GridView.isCurrentItem
                                Rectangle {
                                    id:rec
                                    width: parent.width - AppStyle.mainGapWidth / 2
                                    height: parent.height - AppStyle.mainGapWidth / 2
                                    anchors.top: tAnch ? parent.top : undefined
                                    anchors.bottom: bAnch ? parent.bottom : undefined
                                    anchors.left: lAnch ? parent.left : undefined
                                    anchors.right: rAnch ? parent.right : undefined
                                    anchors.topMargin: tAnch ? 1 : 0
                                    anchors.bottomMargin: bAnch ? 1 : 0
                                    anchors.leftMargin: lAnch ? 1 : 0
                                    anchors.rightMargin: rAnch ? 1 : 0
                                    color: parent.isCurrentItem ? "white" : AppStyle.backColor
                                    radius: AppStyle.radius
                                    border.color:AppStyle.selectedItemColor
                                    border.width: isCurrentItem ? 2 : 0

                                    MouseArea{
                                                anchors.fill:parent
                                                anchors.bottom: parent.bottom
                                                width:parent.width
                                                height: parent.height

                                                hoverEnabled: true
                                                onEntered:  rec.border.width=isCurrentItem ? 4 : 0
                                                onExited:   rec.border.width=isCurrentItem ? 2 : 0
                                                cursorShape: isCurrentItem ? Qt.PointingHandCursor : Qt.ArrowCursor


                                                onClicked:
                                                {


                                                }

                                            }



                                    Item {
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        anchors.top: parent.top
                                        height: parent.height * 2 / 3
                                        //color: "red"

                                        Image {
                                            id: img
                                            opacity: 0.6
                                            anchors.fill: parent
                                            property bool rounded: true
                                            property bool adapt: true
                                            source: photo
                                            fillMode: Image.PreserveAspectFit
                                            layer.enabled: rounded
                                            layer.effect: OpacityMask {
                                                maskSource: Item {
                                                    width: img.width
                                                    height: img.height
                                                    Rectangle {
                                                        anchors.centerIn: parent
                                                        width: img.adapt ? img.width : Math.min(img.width, img.height)
                                                        height: img.adapt ? img.height : width
                                                        radius: AppStyle.radius
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    Item {
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        anchors.bottom: parent.bottom
                                        height: parent.height / 3
                                        //color: "blue"

                                        Column {
                                            id: columnExtranInf
                                            anchors.top: parent.top
                                            anchors.left: parent.left
                                            anchors.leftMargin: AppStyle.mainGapWidth - 10
                                            anchors.right: parent.right
                                            height: parent.height
                                            spacing: 3
                                            Text {
                                                width: parent.width
                                                //height: AppStyle.photoTextHeight
                                                fontSizeMode: Text.HorizontalFit
                                                font.pixelSize: AppStyle.regularTextSize
                                                horizontalAlignment: Text.AlignLeft
                                                verticalAlignment: Text.AlignVCenter
                                                text: qsTr("Модель: ") + modelnumber
                                            }
                                            Text {
                                                width: parent.width
                                                //height: AppStyle.photoTextHeight
                                                fontSizeMode: Text.HorizontalFit
                                                font.pixelSize: AppStyle.regularTextSize
                                                horizontalAlignment: Text.AlignLeft
                                                verticalAlignment: Text.AlignVCenter
                                                text: qsTr("Номер: ") + serialnumber
                                            }
                                            Text {
                                                width: parent.width
                                                //height: AppStyle.photoTextHeight
                                                fontSizeMode: Text.HorizontalFit
                                                font.pixelSize: AppStyle.regularTextSize
                                                horizontalAlignment: Text.AlignLeft
                                                verticalAlignment: Text.AlignVCenter
                                                text: qsTr("Статус: ") + status
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    width: (parent.width - AppStyle.mainGapWidth) / 2
                    color: AppStyle.backColor
                    radius: AppStyle.radius

                    Item {
                        id: files
                        anchors.fill: parent
                        height: parent.height
                        //color: "blue"
                        FilesItem {
                            anchors.fill: parent
                            onItemClicked: {
                                videoPlayer.openFile(name, path)
                            }
                        }
                    }
                }
            }
        }
    }
}
