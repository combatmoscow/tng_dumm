import QtQuick 2.8
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../UI/."

TextField{
    anchors.fill: parent
    verticalAlignment: Qt.AlignVCenter
    horizontalAlignment: Qt.AlignLeft
    font.pixelSize: AppStyle.regularTextSize
    font.family: "helvetica"
    style: TextFieldStyle {
        textColor: "black"
        background: Rectangle {
            radius: 5
            border.color: "#606060"
            border.width: 1
        }
    }
}
