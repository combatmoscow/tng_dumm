import QtQuick 2.8
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "../UI/."
import "../Squad/."

Item {
    id: photoRoot

    signal startStream()

    Item {
        anchors.top: parent.top
        anchors.left: parent.left
        height: (parent.height - AppStyle.extraGapHeight) / 2
        width: (parent.width - AppStyle.extraGapWidth) / 2
        //color: "green"

        Item {
            id: photo
            anchors.fill: parent
            Image {
                id: image
                anchors.fill: parent
                //fillMode: Image.PreserveAspectFit
                //source: Squads.getCurrentModel().get(Squads.)
                source: Squads.getCurrentModel().get(Squads.currentIndex).photo
                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: Item {
                        width: image.width
                        height: image.height
                        Rectangle {
                            anchors.centerIn: parent
                            width: image.width
                            height: image.height
                            radius: AppStyle.radius
                        }
                    }
                }
            }
        }
    }

    Item {
        anchors.top: parent.top
        anchors.right: parent.right
        height: (parent.height - AppStyle.extraGapHeight) / 2
        width: (parent.width - AppStyle.extraGapWidth) / 2
        //color: "blue"

        Item {
            anchors.top: parent.top
            width: parent.width
            height: parent.height / 3
            //color: "green"

            Item {
                anchors.top: parent.top
                width: parent.width
                height: AppStyle.photoTextHeight
                //color: "orange"
                Text {
                    id: budge
                    anchors.fill: parent
                    anchors.leftMargin: 10
                    height: AppStyle.photoTextHeight
                    font.pixelSize: AppStyle.regularTextSize
                    font.bold: true
                    text: "AB123456"
                    font.letterSpacing: parent.width / 40
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                }
            }
            Item {
                anchors.bottom: parent.bottom
                width: parent.width
                height: parent.height - AppStyle.photoTextHeight
                //color: "white"
                Text {
                    anchors.fill: parent
                    anchors.leftMargin: 10
                    height: AppStyle.photoTextHeight
                   font.pixelSize: AppStyle.regularTextSize
                    text: "номер жетона"
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                }
            }
        }







        Item {
            id: item_buts
            anchors.bottom: parent.bottom
            width: parent.width
            height: parent.height * 2 / 3
            //color: "brown"

            Column {
                width: parent.width
                height: AppStyle.photoTextHeight * 4 + spacing * 4
                spacing: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.top: parent.top
                //anchors.verticalCenter: parent.verticalCenter



                         Rectangle
                         {
                             id: rec_button_strem_video

                             width: item_buts.width
                             height: 20

                             anchors.leftMargin: 10

                             radius: AppStyle.radius
                             border.width:1
                             color: marea.pressed ?  "orange" : "lightblue"
                             Text
                             {
                               anchors.fill: parent
                               verticalAlignment: Text.AlignVCenter
                               horizontalAlignment: Text.AlignHCenter // выравнивание текста по горизонтали внутри поля TEXT

                               height: AppStyle.photoTextHeight
                               width: parent.width
                               text: "СТРИМ ВИДЕО"
                             }

                             MouseArea
                             {
                               id:  marea
                               anchors.fill:parent
                               anchors.bottom: parent.bottom
                               width:parent.width
                               height: parent.height

                               hoverEnabled: true
                               onEntered:  rec_button_strem_video.border.width=2
                               onExited:  rec_button_strem_video.border.width=1
                               cursorShape:Qt.PointingHandCursor


                              onClicked:
                              {
                                 startStream()
                              }

                             }


                          }


                         Rectangle
                         {
                             id: rec_button_strem_audio

                             width: item_buts.width
                             height: 20

                             anchors.leftMargin: 10

                             radius: AppStyle.radius
                             border.width:1
                             color: marea_audio.pressed ?  "orange" : "lightblue"
                             Text
                             {
                               anchors.fill: parent
                               verticalAlignment: Text.AlignVCenter
                               horizontalAlignment: Text.AlignHCenter // выравнивание текста по горизонтали внутри поля TEXT

                               height: AppStyle.photoTextHeight
                               width: parent.width
                               text: "СТРИМ АУДИО"
                             }

                             MouseArea
                             {
                               id:  marea_audio
                               anchors.fill:parent
                               anchors.bottom: parent.bottom
                               width:parent.width
                               height: parent.height

                               hoverEnabled: true
                               onEntered:  rec_button_strem_audio.border.width=2
                               onExited:  rec_button_strem_audio.border.width=1
                               cursorShape:Qt.PointingHandCursor


                              onClicked:
                              {

                              }

                             }


                          }

                         Rectangle
                         {
                             id: rec_button_strem_foto
                             width: item_buts.width
                             height: 20

                             anchors.leftMargin: 10

                             radius: AppStyle.radius
                             border.width:1
                             color: marea_foto.pressed ?  "orange" : "lightblue"
                             Text
                             {
                               anchors.fill: parent
                               verticalAlignment: Text.AlignVCenter
                               horizontalAlignment: Text.AlignHCenter // выравнивание текста по горизонтали внутри поля TEXT

                               height: AppStyle.photoTextHeight
                               width: parent.width
                               text: "СТРИМ ФОТО"
                             }

                             MouseArea
                             {
                               id:  marea_foto
                               anchors.fill:parent
                               anchors.bottom: parent.bottom
                               width:parent.width
                               height: parent.height

                               hoverEnabled: true
                               onEntered:  rec_button_strem_foto.border.width=2
                               onExited:  rec_button_strem_foto.border.width=1
                               cursorShape:Qt.PointingHandCursor


                              onClicked:
                              {

                              }

                             }


                          }


//                Item {
//                    height: AppStyle.photoTextHeight
//                    width: parent.width / 1.3
//                    PhotoTextField {
//                        text: Squads.getCurrentModel().get(Squads.currentIndex).name
//                    }
//                }
//                Item {
//                    height: AppStyle.photoTextHeight
//                    width: parent.width / 1.3
//                    PhotoTextField {
//                        text: Squads.getCurrentModel().get(Squads.currentIndex).patriot
//                    }
//                }
//                Item {
//                    height: AppStyle.photoTextHeight
//                    width: parent.width / 1.3
//                    PhotoTextField {
//                        text: "22/10/1983"
//                    }
                //}
            }
        }

    }

    Item {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: (parent.height - AppStyle.extraGapHeight) / 2
        //color: AppStyle.backColor

        Item {
            id: extra_inf
            anchors.fill: parent
            anchors.topMargin: 10

            Column {
                id: columnExtranInf
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width / 3
                height: parent.height
                spacing: 2
                PhotoText
                {
                    text: qsTr("Отдел")                    
                }
                PhotoText
                {
                    text: qsTr("Должность")
                }
                PhotoText
                {
                    text: qsTr("Номер телефона")
                }
                PhotoText
                {
                    text: qsTr("Адрес почты")
                }
                PhotoText
                {
                    text: qsTr("Рабочая зона")
                }
                PhotoText
                {
                    text: qsTr("Табельный номер")
                }
            }

            Column {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: columnExtranInf.right
                anchors.right: parent.right
                spacing: 2

                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).department
                    }
                }
                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).position
                    }
                }
                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).phone
                    }
                }
                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).email
                    }
                }
                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).zone
                    }
                }
                Item {
                    height: AppStyle.photoTextHeight
                    width: parent.width
                    PhotoTextField {
                        text: Squads.getCurrentModel().get(Squads.currentIndex).servicenumber
                    }
                }
            }
        }
    }
}
