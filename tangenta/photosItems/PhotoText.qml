import QtQuick 2.8
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../UI/."

Text {
    width: parent.width
    height: AppStyle.photoTextHeight
    fontSizeMode: Text.HorizontalFit
   font.pixelSize: AppStyle.regularTextSize
    horizontalAlignment: Text.AlignLeft
    verticalAlignment: Text.AlignVCenter
}
