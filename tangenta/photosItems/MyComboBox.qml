import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

ComboBox {
    id: box
    currentIndex: 0
    activeFocusOnPress: true

    style: ComboBoxStyle {
        id: comboBox
        background: Rectangle {
            id: rectCategory
            //radius: 5
            border.width: 1
            border.color: "lightblue"
            color: "#fff"

            Item {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                width: parent.width / 7
            }
        }

        label: Text {
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 14
            text: control.currentText
            color: "black"
            //horizontalAlignment: Text.AlignHCenter
            //font.family: "Courier"
            //font.capitalization: Font.SmallCaps
        }

        // drop-down customization here
        property Component __dropDownStyle: MenuStyle {
            __maxPopupHeight: 600
            __menuItemType: "comboboxitem"
            frame: Rectangle {              // background
                color: "white"
                border.width: 1
                //radius: 5
            }

            itemDelegate.label:             // an item text
                Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 14
                //font.family: "Courier"
                //font.capitalization: Font.SmallCaps
                color: styleData.selected ? "white" : "black"
                text: styleData.text
            }

            itemDelegate.background: Rectangle {  // selection of an item
                radius: 2
                color: styleData.selected ? "#3A83A1" : "transparent"
            }

            __scrollerStyle: ScrollViewStyle { }
        }

        property Component __popupStyle: Style {
            property int __maxPopupHeight: 400
            property int submenuOverlap: 0

            property Component frame: Rectangle {
                width: (parent ? parent.contentWidth : 0)
                height: (parent ? parent.contentHeight : 0) + 2
                border.color: "black"
                property real maxHeight: 500
                property int margin: 1
            }

            property Component menuItemPanel: Text {
                text: "NOT IMPLEMENTED"
                color: "red"
                font {
                    pixelSize: 14
                    bold: true
                }
            }

            property Component __scrollerStyle: null
        }
    }
}
