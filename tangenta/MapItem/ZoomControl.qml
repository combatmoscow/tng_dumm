import QtQuick 2.7
import "../UI/."

Item {
    id: zoomControlRoot
    height: controlsColumn.height
    property int spacing: 0
    property int currentZoomLevel
    signal zoomPlusClicked()
    signal zoomMinusClicked()

    Column {
        id: controlsColumn
        width: parent.width
        spacing: parent.spacing
        Image {
            id: plusButton
            width: parent.width
            height: width
            source: "plus_button.png"
            MouseArea {
                id: plusMouseArea
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    zoomControlRoot.zoomPlusClicked()
                }
            }
            Rectangle {                 // border
                anchors.fill: parent
                radius: 50
                color: "transparent"
                border.color: "white"
                border.width: 1.5
                opacity: plusMouseArea.containsMouse ? 1 : 0
                Behavior on opacity {
                    NumberAnimation { duration: 100 }
                }
            }
        }
        Image {
            id: currentZoomLevel
            width: parent.width
            height: width
            source: "empty_button.png"
            Text {
                anchors.centerIn: parent
                font.pixelSize: AppStyle.smallTextSize
                text: zoomControlRoot.currentZoomLevel
                color: "white"
            }
        }
        Image {
            id: minusButton
            width: parent.width
            height: width
            source: "minus_button.png"
            MouseArea {
                id: minusMouseArea
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    zoomControlRoot.zoomMinusClicked()
                }
            }
            Rectangle {                 // border
                anchors.fill: parent
                radius: 50
                color: "transparent"
                border.color: "white"
                border.width: 1.5
                opacity: minusMouseArea.containsMouse ? 1 : 0
                Behavior on opacity {
                    NumberAnimation { duration: 100 }
                }
            }
        }
    }
}
