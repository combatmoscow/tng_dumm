import QtQuick 2.8
import QtQuick.Window 2.2

import "../Squad/."

Rectangle {
    id: rootControlPanel
    Menu {
        id: menu
        anchors.centerIn: parent
        itemWidth: parent.width
        itemHeight: 30

        spacing: 0
        model: ListModel {
            ListElement { name: qsTr("Отряд 1")}
            ListElement { name: qsTr("Отряд 2")}
            ListElement { name: qsTr("Отряд 3")}
            ListElement { name: qsTr("Отряд 4")}
            ListElement { name: qsTr("Отряд 5")}
        }
        onClicked: {
            console.log(" index = " + index)
            Squads.setCurrentIndex(0)
            Squads.setCurrentModel(index)
            //selectedX = x
            //selectedY = y
            //selectedUrl = model.get(index).url
            //mapRoot.currentTroop = mapRoot.troops[index]
            //root.clicked()
        }
    }
    
    state: "hide"
    states: [
        State {
            name: "show"
            PropertyChanges { target: rootControlPanel; opacity: 1; anchors.rightMargin: 0 }
        },
        State {
            name: "hide"
            PropertyChanges { target: rootControlPanel; opacity: 0; anchors.rightMargin: -rootControlPanel.width }
        }
    ]
    transitions: [
        Transition {
            from: "*"; to: "*"
            PropertyAnimation {
                properties: "opacity,anchors.rightMargin"
                easing.type: Easing.OutQuart
                duration: 1000
            }
        }
    ]
}
