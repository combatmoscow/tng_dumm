function createMap(name, mapZoom, mapCenter) {
    var _name = "map"
    var _zoom = 10
    var _center = [55.754944, 37.617990]
    if (mapZoom !== undefined) {
        _zoom = mapZoom
    }
    if (mapCenter !== undefined) {
        _center = mapCenter
    }
    if (name !== undefined) {
        _name = name
    }

    var map =  new ymaps.Map(_name, {
        center: _center,
        zoom: _zoom
    });

    map.controls.add('zoomControl', {size: "auto"});
    map.controls.add('rulerControl');
    return map
}

function pointInArea(leftTop, rightBottom, point) {

    return (leftTop[0] - 0.04 <= point[0]              &&
            point[0]          <= rightBottom[0] + 0.04 &&
            leftTop[1] - 0.03 <= point[1]              &&
            point[1]          <= rightBottom[1] + 0.03)
}

function createMark(markItem, height) {
    var _height = 35 // default height
    if (height !== undefined) {
        _height = height
    }
    var mark = new ymaps.Placemark(markItem.point,
        {
            balloonContent: makeBalloonContent(markItem)
        },
        {
            iconLayout: 'default#image',
            iconImageHref: 'file://' + markItem.image, // + /home/bisection/Work/Images/2.png',
            iconImageSize: [_height, _height],
            pointId: markItem.id,
            hideIconOnBalloonOpen: false,
            balloonOffset: [0, -_height]
        })
    return mark
}

function createLine(coords, color, name) {
    var newLine = new  ymaps.Polyline(coords,
        {
            hintContent: name
        },
        {
            draggable: false,
            strokeColor: color,
            strokeWidth: 4
        }
    )
    return newLine
}

function makeBalloonContent(mark) {
    var str = ""
    str = "<style type=\"text/css\">" +
        "#maket {" +
        "width: 100%;" +
        "}" +
        "TD {" +
        "vertical-align: top; /* Вертикальное выравнивание в ячейках */" +
        "padding: 5px; /* Поля вокруг ячеек */" +
        "}" +
        "TD.fistcol {" +
        "width: auto;" +
        "}" +
        "TD.simpleColumn {" +
        "width: auto;" +
        "text-align: left;" +
        "white-space: nowrap;" +
        "}" +
        "TD.ruColumn {" +
        "width: auto;" +
        "text-align: right;" +
        "white-space: nowrap;" +
        "}" +
        "</style>";
    str += "<a href=\"javascript:void(0)\">Изменить формат базы</a><br>"

    str += "<table cellspacing=\"0\" id=\"maket\">";
    str += "<tr>";
    str += "<td class=\"firstcol\">" + mark.indexesColumn + "</td>";
    str += "<td class=\"simpleColumn\">" + mark.enTitlesColumn + "</td>";
    str += "<td class=\"ruColumn\">" + mark.ruTitlesColumn + "</td>";
    str += "<td class=\"simpleColumn\">" + mark.valuesColumn + "</td>";
    str += "</tr>";
    str += "</table>";
    str += "<a href=\"javascript:void(0)\">Изменить данные объекта</a><br>"
    str += "<a href=\"javascript:void(0)\">Голосовать за рейтинг</a><br>"
    str += "<a href=\"javascript:void(0)\">Фотографии объекта</a>"
    str += "<center><img src=\"qrc:/dist/mark/like.jpg\" alt=\".\" style=\"width:30px;height:30px;\">"
    str += "<img src=\"qrc:/dist/mark/dislike.jpg\" alt=\".\" style=\"width:30px;height:30px;\"></center>"
    return str
}

function generateColors(count) {
    var colors = []
    for (var colorIndex = 0; colorIndex < count; ++colorIndex) {
        var h = 360 / count * colorIndex
        colors.push(hslToRgb(h, 100, 50))
    }
    return colors
}

function hslToRgb(h, s, l) {
    s = s / 100
    l = l / 100
    var c = (1 - Math.abs(2 * l - 1) ) * s;
    var x = c * (1 - Math.abs((h / 60)%2 - 1))
    var m = l - c/2
    var r_, g_, b_;
    var R, G, B
    if (0   <= h && h < 60 ) { r_ = c; g_ = x; b_ = 0; }
    if (60  <= h && h < 120) { r_ = x; g_ = c; b_ = 0; }
    if (120 <= h && h < 180) { r_ = 0; g_ = c; b_ = x; }
    if (180 <= h && h < 240) { r_ = 0; g_ = x; b_ = c; }
    if (240 <= h && h < 300) { r_ = x; g_ = 0; b_ = c; }
    if (300 <= h && h < 360) { r_ = c; g_ = 0; b_ = x; }
    R = Math.round((r_ + m) * 255)
    G = Math.round((g_ + m) * 255)
    B = Math.round((b_ + m) * 255)
    var Rs, Gs, Bs
    Rs = Gs = Bs = ""
    if (R < 17) Rs += "0"
    if (G < 17) Gs += "0"
    if (B < 17) Bs += "0"
    Rs += R.toString(16)
    Gs += G.toString(16)
    Bs += B.toString(16)
    return "#" + Rs + Gs + Bs
}

function findHitPois(pois, carPos, carAz) {
    /*
    console.log("-----------------")
    console.log("FindHitPois")
    console.log("Car pos: " + carPos)
    console.log("CarAz: " + carAz)
    console.log("Pois count: " + pois.length)
    */
    var result = []
    var bear = 0
    var poi_coord
    var dist_AB
    var angle_half
    for (var i in pois) {
        poi_coord = QtPositioning.coordinate(pois[i].lat, pois[i].lon)
        bear = poi_coord.azimuthTo(carPos)
        angle_half = pois[i].angle * .5
        dist_AB = poi_coord.distanceTo(carPos)
        /*
        console.log("Bear: " + bear)
        console.log("Distance betweein points: " + dist_AB)
        console.log("POI length: " + pois[i].distance)
        console.log("POI direction: " + pois[i].az)
        console.log("POI angle: " + pois[i].angle)
        console.log("POI: " + JSON.stringify(pois[i]))
        console.log("1: " + (bear > (pois[i].az - angle_half)))
        console.log("2: " + (bear < (pois[i].az + angle_half)))
        console.log("3: " + (dist_AB < Math.floor(pois[i].distance)))
        console.log("4: " +  (Math.abs(pois[i].az - Math.ceil(carAz)) > 120))
        */
        var newPoint
        if ( (bear > (pois[i].az - angle_half)) &&
             (bear < (pois[i].az + angle_half)) &&
             (dist_AB < Math.floor(pois[i].distance))          &&
             (Math.abs(pois[i].az - Math.ceil(carAz)) > 120)
                ) {
//                console.log("ADDED!")
            newPoint = pois[i]
            newPoint["distance_to"] = distanceBetweenPoints(newPoint, carPos)
            result.push(newPoint)
            continue
        }

        if (pois[i].dir_type === 2) // дополнительно проверяем заднюю сторону
        {
            angle_half = pois[i].angle * 0.5;
            if((pois[i].az >= 0) && (pois[i].az < 180))
            {
                if ( (bear > (pois[i].az + 180.0 - angle_half)) &&
                        (bear < (pois[i].az + 180.0 + angle_half)) &&
                        (dist_AB < pois[i].az)            )
                {
                    // для задней части курс не важен
                    //				if (( abs(item->direct + 180 - (u32)ceil(curs)) < 60 ) ||
                    //					( abs(item->direct       - (u32)ceil(curs)) > 120)    )
                    newPoint = pois[i]
                    newPoint["distance_to"] = distanceBetweenPoints(newPoint, carPos)
                    result.push(newPoint)
                }
            }
            else if ((pois[i].az >= 180) && (pois[i].az < 360))
            {
                if ( (bear > (pois[i].az - 180.0 - angle_half)) &&
                        (bear < (pois[i].az- 180.0 + angle_half)) &&
                        (dist_AB < pois[i].distance)            )
                {
                    // для задней части курс не важен
                    newPoint = pois[i]
                    newPoint["distance_to"] = distanceBetweenPoints(newPoint, carPos)
                    result.push(newPoint)
                }
            }
        }
    }
    return result
}

function distanceBetweenPoints(point, coord) {
    var pointCoord = QtPositioning.coordinate(point.lat, point.lon)
    return pointCoord.distanceTo(coord)
}

function getRatioByZoomLevel(zoom) {
    if (17.934 < zoom) return 2.99342993335894
    if (17.9   < zoom && zoom <= 17.934) return 2.92776560661162
    if (17.866 < zoom && zoom <= 17.9  ) return 2.8595748382698
    if (17.834 < zoom && zoom <= 17.866) return 2.79297233464035
    if (17.8   < zoom && zoom <= 17.834) return 2.73170535513493
    if (17.766 < zoom && zoom <= 17.8  ) return 2.66808112246191
    if (17.734 < zoom && zoom <= 17.766) return 2.60593879306144
    if (17.7   < zoom && zoom <= 17.734) return 2.54877468338594
    if (17.666 < zoom && zoom <= 17.7  ) return 2.48941117885142
    if (17.634 < zoom && zoom <= 17.666) return 2.43143033889773
    if (17.6   < zoom && zoom <= 17.634) return 2.3780943419024
    if (17.566 < zoom && zoom <= 17.6  ) return 2.32270623707296
    if (17.534 < zoom && zoom <= 17.566) return 2.26860820378463
    if (17.5   < zoom && zoom <= 17.534) return 2.218843962013
    if (17.466 < zoom && zoom <= 17.5  ) return 2.1671650360172
    if (17.434 < zoom && zoom <= 17.466) return 2.11668978929615
    if (17.4   < zoom && zoom <= 17.434) return 2.07025811275634
    if (17.366 < zoom && zoom <= 17.4  ) return 2.02203997278209
    if (17.334 < zoom && zoom <= 17.366) return 1.97494490522386
    if (17.3   < zoom && zoom <= 17.334) return 1.93162262178046
    if (17.266 < zoom && zoom <= 17.3  ) return 1.88663350917266
    if (17.234 < zoom && zoom <= 17.266) return 1.84269226008024
    if (17.2   < zoom && zoom <= 17.234) return 1.80227114281277
    if (17.166 < zoom && zoom <= 17.2  ) return 1.76029481906422
    if (17.134 < zoom && zoom <= 17.166) return 1.71929618649401
    if (17.1   < zoom && zoom <= 17.134) return 1.68158195285204
    if (17.066 < zoom && zoom <= 17.1  ) return 1.64241666030639
    if (17.034 < zoom && zoom <= 17.066) return 1.60416358584136
    if (17     < zoom && zoom <= 17.034) return 1.56897496376515
    if (16.968 < zoom && zoom <= 17    ) return 1.53243248774335
    if (16.934 < zoom && zoom <= 16.968) return 1.49881477550319
    if (16.9   < zoom && zoom <= 16.934) return 1.46390643347556
    if (16.868 < zoom && zoom <= 16.9  ) return 1.42981115809371
    if (16.834 < zoom && zoom <= 16.868) return 1.39844722431107
    if (16.8   < zoom && zoom <= 16.834) return 1.36587656352505
    if (16.768 < zoom && zoom <= 16.8  ) return 1.33406452032429
    if (16.734 < zoom && zoom <= 16.768) return 1.30480093714876
    if (16.7   < zoom && zoom <= 16.734) return 1.27441143793939
    if (16.668 < zoom && zoom <= 16.7  ) return 1.24472975393614
    if (16.634 < zoom && zoom <= 16.668) return 1.21742586704552
    if (16.6   < zoom && zoom <= 16.634) return 1.18907146342104
    if (16.568 < zoom && zoom <= 16.6  ) return 1.16137747469931
    if (16.534 < zoom && zoom <= 16.568) return 1.1359020490056
    if (16.5   < zoom && zoom <= 16.534) return 1.10944645658901
    if (16.468 < zoom && zoom <= 16.5  ) return 1.08360705303514
    if (16.434 < zoom && zoom <= 16.468) return 1.05983764183998
    if (16.4   < zoom && zoom <= 16.434) return 1.03515370282715
    if (16.368 < zoom && zoom <= 16.4  ) return 1.01104468829475
    if (16.334 < zoom && zoom <= 16.368) return 0.988867044831851
    if (16.3   < zoom && zoom <= 16.334) return 0.965836116787035
    if (16.268 < zoom && zoom <= 16.3  ) return 0.943341612218602
    if (16.234 < zoom && zoom <= 16.268) return 0.922649140461162
    if (16.2   < zoom && zoom <= 16.234) return 0.901160526088642
    if (16.168 < zoom && zoom <= 16.2  ) return 0.88017241249262
    if (16.134 < zoom && zoom <= 16.168) return 0.860865654852822
    if (16.1   < zoom && zoom <= 16.134) return 0.840816069934193
    if (16.068 < zoom && zoom <= 16.1  ) return 0.82123346872173
    if (16.034 < zoom && zoom <= 16.068) return 0.803219627998307
    if (16     < zoom && zoom <= 16.034) return 0.784512704952937
    if (15.968 < zoom && zoom <= 16    ) return 0.766241493091234
    if (15.934 < zoom && zoom <= 15.968) return 0.749432902899277
    if (15.9   < zoom && zoom <= 15.934) return 0.731978751949083
    if (15.868 < zoom && zoom <= 15.9  ) return 0.714931134202765
    if (15.834 < zoom && zoom <= 15.868) return 0.699249200495316
    if (15.8   < zoom && zoom <= 15.834) return 0.682963902821047
    if (15.768 < zoom && zoom <= 15.8  ) return 0.667057914025012
    if (15.734 < zoom && zoom <= 15.768) return 0.652426153407814
    if (15.7   < zoom && zoom <= 15.734) return 0.637231434342092
    if (15.668 < zoom && zoom <= 15.7  ) return 0.622390622948705
    if (15.634 < zoom && zoom <= 15.668) return 0.608738708415882
    if (15.6   < zoom && zoom <= 15.634) return 0.594561535112726
    if (15.568 < zoom && zoom <= 15.6  ) return 0.580714569321094
    if (15.534 < zoom && zoom <= 15.568) return 0.567976883455052
    if (15.5   < zoom && zoom <= 15.534) return 0.554749113857803
    if (15.468 < zoom && zoom <= 15.5  ) return 0.541829438745802
    if (15.434 < zoom && zoom <= 15.468) return 0.529944758338006
    if (15.4   < zoom && zoom <= 15.434) return 0.517602813667219
    if (15.368 < zoom && zoom <= 15.4  ) return 0.505548331301462
    if (15.334 < zoom && zoom <= 15.368) return 0.494459542740317
    if (15.3   < zoom && zoom <= 15.334) return 0.482944111704078
    if (15.268 < zoom && zoom <= 15.3  ) return 0.471696882210449
    if (15.234 < zoom && zoom <= 15.268) return 0.461350667302287
    if (15.2   < zoom && zoom <= 15.234) return 0.45060638189539
    if (15.168 < zoom && zoom <= 15.2  ) return 0.44011234637388
    if (15.134 < zoom && zoom <= 15.168) return 0.430458987134306
    if (15.1   < zoom && zoom <= 15.134) return 0.420434215010701
    if (15.068 < zoom && zoom <= 15.1  ) return 0.410642934273021
    if (15.034 < zoom && zoom <= 15.068) return 0.401636032192149
    if (zoom <= 15.034) return 0.392282589660311
}

