import QtQuick 2.0
import "../UI/."

Rectangle {
    id: root
    property color selectedColor: AppStyle.selectedItemColor
    property alias text: itemText.text
    color: "#00000000"
    signal clicked
    Text {
        id: itemText
        //color: "white"
        anchors.fill: parent
        anchors.margins: 4
        font.pixelSize: AppStyle.smallTextSize
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.state = "selected"
            root.clicked()
        }
    }
    states: [
        State {
            name: "selected"
            PropertyChanges {
                target: delegateItem
                color: selectedColor
            }
        }
    ]
    transitions: [
        Transition {
            from: "*"; to: "*"
            ColorAnimation {
                properties: "color"
                easing.type: Easing.OutQuart
                duration: 500
            }
        }
    ]
}
