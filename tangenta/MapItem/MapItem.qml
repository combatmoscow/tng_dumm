import QtQuick 2.5
import QtLocation 5.5
import QtPositioning 5.6
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.4

import "../UI/."
import "../Squad/."

import "maphelper.js" as MapHelper
//import "testTracks.js" as TestTracks


Map {
    id: mapRoot
    copyrightsVisible: false
    property int poiOpacityDur: 500
    property int poiScaleDur: 200

    activeMapType:supportedMapTypes[5]
    plugin: mapPlugin

    layer.enabled: true
    layer.effect: OpacityMask {
        maskSource: Item {
            width: mapRoot.width
            height: mapRoot.height
            Rectangle {
                anchors.centerIn: parent
                width: mapRoot.width
                height: mapRoot.height
                radius: AppStyle.radius
            }
        }
    }


    Plugin {
          id:mapPlugin
          allowExperimental: true
          //name: SettingsManager.mapIndex == 0 ? "yandex" : "osm"
          name: "osm"


      }




    property variant region: undefined
    function updateRegion() {
        return QtPositioning.rectangle(toCoordinate(Qt.point(-mapRoot.width / 5, -mapRoot.height / 5), false),
                                       toCoordinate(Qt.point(mapRoot.width + mapRoot.width / 5, mapRoot.height + mapRoot.height / 5), false))
    }
    Component.onCompleted: {
        mapRoot.region = updateRegion()
        mapItem.showRoutes()
    }

    property int zoomDefault: 15
    zoomLevel: zoomDefault

    onZoomLevelChanged: {
        mapRoot.region = updateRegion()
    }

    //center: QtPositioning.coordinate(55.82179426, 37.64240468) // Moscow
 //   center: QtPositioning.coordinate(56.835764, 60.612993)   // Ekb
    center: QtPositioning.coordinate(41.303708, 69.160159)   // Tachkent

    property int lonAndLatDuration: 2000
    Behavior on center {
      CoordinateAnimation {
        duration: lonAndLatDuration
       }
    }

    function getCurrentCenterCoordinate() {
        var coords = {}
        coords.lat = mapRoot.center.latitude
        coords.lon = mapRoot.center.longitude
        return coords
    }

//    PositionSource {
//        id: positionDetect
//        updateInterval: 1000
//        active: true
//        onPositionChanged: {
//            var coord = positionDetect.position.coordinate;
//            console.log("Coordinate:", coord.longitude, coord.latitude)
//            if(isNaN(coord.longitude) && isNaN(coord.latitude)) {
//                console.log(" MapItem coords NaN ")
//            } else {
//                mapRoot.center = QtPositioning.coordinate(coord.latitude, coord.longitude)
//            }
//            positionDetect.active = false
//        }
//    }

    function videoPlayFinished() {      // вызывается при окончании видео
        console.log(" MapItem videoPlayFinished ")
        resetMoveAnimation()
    }

    function findCurrentLocation() {
        mapRoot.center = QtPositioning.coordinate(carMark.coordinate.latitude, carMark.coordinate.longitude)
    }


//---------------------------- work with car -------------------------//
    property int carMoveAnimDur: carMoveAnimDurDefault
    property int carMoveAnimDurDefault: 2000

    MapQuickItem {
        id: carMark
        anchorPoint.x: rectItem.width / 2
        anchorPoint.y: rectItem.height / 2
        clip: false
        z: 1499
        visible: false
        coordinate: QtPositioning.coordinate(55.82179426, 37.64240468) // Moscow

        Behavior on coordinate {
            CoordinateAnimation {
                duration: carMoveAnimDur
            }
        }
        sourceItem: Rectangle {
            id: rectItem
            height: 20
            width: 20
            radius: 50
            color: "#4F58BF"
            opacity: 0.8
        }
    }

    function showCarMark() {
        carMark.visible = true
    }

    function hideCarMark() {
        carMark.visible = false
    }

    function updateCarRotation(pos) {
        var initCoord = QtPositioning.coordinate(pos.prevCoords[0], pos.prevCoords[1])
        var lastCoord = QtPositioning.coordinate(pos.coords[0], pos.coords[1])
        var azimuth = initCoord.azimuthTo(lastCoord)
        carMark.rotation = azimuth
    }

    function updateCarPosition(pos) {
        carMark.coordinate = QtPositioning.coordinate(pos.coords[0], pos.coords[1])
    }
//---------------------------- work with car -------------------------//


//--------------------------- line item begin ------------------------ //
    property int currentTrack: -1
    function setCurrentTrack(trackId) {
        console.log(" trackId = " + trackId)
        currentTrack = trackId
        resetMoveAnimation()
        //startPreLoadCurrentTrack()
    }

    Component {
        id: lineComponent
        MapPolyline {
            id: line
            opacity: 0.8
            objectName: "line_item"
            line.width: 3
            smooth: true
            property int index
            antialiasing: true
        }
    }

    function showRoutes(tracks, ids) {
        // Функция вызывалась при переключении даты в календаре
        // В нашем приложении по умолчанию загрузятся все 5 треков
        var tracksInfo = Manager.getAllTracks();
        clearRoutes()
        if (tracksInfo.tracks.length <= 0) {
            return
        }
        var colors = MapHelper.generateColors(tracksInfo.tracks.length)
        console.log(" MapItem.qml showRoutes tracksInfo.tracks.length = " + tracksInfo.tracks.length)
        for (var i = 0; i < tracksInfo.tracks.length; ++i) {
            addLineItem(tracksInfo.tracks[i], "#D72323", tracksInfo.ids[i])
        }
    }

    function addLineItem(coords, color, id) {
        var newLine = lineComponent.createObject(mapRoot)
        newLine.line.color = color
        newLine.index = id
        console.log(" addLineItem coords.length = " + coords.length + "   id = " + id)
        for (var i = 0; i < coords.length; ++i) {
            //console.log(" lat = " + coords[i][0] + " lon = " + coords[i][1])
            newLine.addCoordinate(QtPositioning.coordinate(coords[i][0], coords[i][1]))
        }
        addMapItem(newLine)
    }

    function clearRoutes() {
        for (var i = 0; i < mapItems.length;) {
            if (mapItems[i].objectName === "line_item") {
                removeMapItem(mapItems[i])
            } else {
                i++;
                continue;
            }
        }
    }
//--------------------------- line item end ------------------------ //

// -------------------------- zoom control -------------------------//

    ZoomControl {
        id: zoomControl
        currentZoomLevel: zoomLevel
        width: 25
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottomMargin: 5
        spacing: 5
        onZoomPlusClicked: {
            increaseZoom()
        }
        onZoomMinusClicked: {
            decreaseZoom()
        }
    }

    function increaseZoom() {
        if (zoomLevel >= maximumZoomLevel) {
            return
        }
        zoomLevel = zoomLevel + 1
    }
    function decreaseZoom() {
        if (zoomLevel <= minimumZoomLevel) {
            return
        }
        zoomLevel = zoomLevel - 1
    }
// -------------------------- zoom control -------------------------//

    function updateCarPos(pos) {
        console.log(" Update car pos: " + JSON.stringify(pos))
        updateCarPosition(pos)
        if(pos.prevCoords != undefined) {   // самый первый элемент prevCoords не передается
            updateCarRotation(pos)
        }
        carMoveAnimDur = carMoveAnimDurDefault
        mapRoot.center = QtPositioning.coordinate(pos.coords[0], pos.coords[1])
    }

    function resetMoveAnimation() {
        carMoveAnimDur = 0
    }

//------------------------ preload selected track begin --------------------//
    function startPreLoadCurrentTrack() {
        zoomLevel = 16
        lonAndLatDuration = 0
        getLastIndexPreLoadMap()
        preLoadMapAnim.start()
    }

    property var cooridanePreLoadMap
    property int indexPreLoadMap

    SequentialAnimation {
        id: preLoadMapAnim
        running: false
        loops: Animation.Infinite

        ScriptAction {
            script: {
                getNextIndexPreLoadMap();
            }
        }
        ScriptAction {  script: mapRoot.center = QtPositioning.coordinate(cooridanePreLoadMap.latitude,
                                                                          cooridanePreLoadMap.longitude) }
        PauseAnimation { duration: 5 }
        ScriptAction {  script: {
                if(indexPreLoadMap == 0) {
                    console.log(" indexPreLoadMap >= 0 ")
                    preLoadMapAnim.running = false
                    lonAndLatDuration = 2000
                }
            } }
    }

    function getLastIndexPreLoadMap() {
        for (var i = 0; i < mapItems.length; i++) {
            if (mapItems[i].objectName === "line_item" && mapItems[i].index == currentTrack) {
                indexPreLoadMap = mapItems[i].pathLength() - 1
            }
        }
    }

    function getNextIndexPreLoadMap() {
        for (var i = 0; i < mapItems.length; i++) {
            if (mapItems[i].objectName === "line_item" && mapItems[i].index == currentTrack) {
                //console.log(" indexPreLoadMap =  " + indexPreLoadMap)
                if(indexPreLoadMap >= 0) {
                    cooridanePreLoadMap = mapItems[i].coordinateAt(indexPreLoadMap)
                }
                indexPreLoadMap--
            }
        }
    }
//------------------------ preload selected track end --------------------//


//    function changeEmp(currentEmployee) {
//        console.log("mapItem changeEmp name = "  + currentEmployee.name   )
//        console.log("mapItem changeEmp number = "  + currentEmployee.number)
//        userPoiModel.get(currentEmployee.number - 1).isOnMap = false
//    }

    MapItemView {
        model: Squads.getCurrentModel()
        delegate: MapQuickItem {
            anchorPoint.x: rectItemEmp.width / 2
            anchorPoint.y: rectItemEmp.height / 2
            coordinate: QtPositioning.coordinate(lat, lon)
            z: 1499
            visible: isOnMap
            Behavior on coordinate {
                CoordinateAnimation {
                    duration: carMoveAnimDur
                }
            }
            sourceItem: Rectangle {
                id: rectItemEmp
                height: 20
                width: 20
                radius: 50
                color: troopColor
                opacity: 0.8
            }
        }
    }

    Connections {
        target: Client
        ignoreUnknownSignals: true
        onGeoUpdated: {
            console.log(" MapItem lat = " + coords[0] + " lon = " + coords[1])
            console.log(" MapItem index = " + index)
            var pos = [parseFloat(coords[0]), parseFloat(coords[1])]
            updateTripleGspItem(pos, index)
        }
    }

//    Timer {
//        interval: 5000; running: true; repeat: true
//        onTriggered: {
//            console.log(" This is for test. Before updateTripleGspItem for 0 item ")
//            var pos = [41.324802, 69.163153]
//            updateTripleGspItem(pos, 0)
//        }
//    }

    function updateTripleGspItem(pos, index) {          // index [0, 2]
        console.log(" updateTripleGspItem ")
        Squads.getTripleGps().get(index).lat = pos[0]
        Squads.getTripleGps().get(index).lon = pos[1]
    }

    MapItemView {
        model: Squads.getTripleGps()
        delegate: MapQuickItem {
            anchorPoint.x: rectTripleGps.width / 2
            anchorPoint.y: rectTripleGps.height / 2
            coordinate: QtPositioning.coordinate(lat, lon)
            z: 1499
            visible: isOnMap
            Behavior on coordinate {
                CoordinateAnimation {
                    duration: carMoveAnimDur
                }
            }
            sourceItem: Rectangle {
                id: rectTripleGps
                height: 20
                width: 20
                radius: 50
                color: troopColor
                opacity: 1

                Text
                {
                   anchors.right: parent.left
                    //anchors.rightMargin: 10
                    //horizontalAlignment: Text.AlignRight
                    text: number

                    font.pixelSize: 15
                }
            }
        }
    }
}
