#include "GpsItem.hpp"

GpsItem::GpsItem() { }

void GpsItem::setBaseName(QString name) {
    mBaseName = name;
}

void GpsItem::setCoords(QList<Coord> coords) {
    mCoords = coords;
    foreach (Coord coord, coords) {
        QVariantList point;
        point.append(coord.lat);
        point.append(coord.lon);
        mCoordsList.append(QVariant(point));
    }
}
