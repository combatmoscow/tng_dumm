#include <QtNetwork>
#include "client.h"
#include <QDebug>

#define TEST_IPADRESS_FIRST QString("192.168.1.40")
#define TEST_IPADRESS_SECOND QString("192.168.1.40")
#define TEST_IPADRESS_THIRD QString("192.168.1.40")
#define TEST_PORT_FIRST 5055
#define TEST_PORT_SECOND 5056
#define TEST_PORT_THIRD 5057

Client::Client(QObject *parent) : QObject(parent)
{
    for (int i = 0; i < 3; i++) {
        listSockets.append(new QTcpSocket());
    }
//    listAdresses.append(TEST_IPADRESS_FIRST);
//    listAdresses.append(TEST_IPADRESS_SECOND);
//    listAdresses.append(TEST_IPADRESS_THIRD);
//    listPorts.append(TEST_PORT_FIRST);
//    listPorts.append(TEST_PORT_SECOND);
//    listPorts.append(TEST_PORT_THIRD);
    connect(listSockets.at(0), &QIODevice::readyRead, this, &Client::readGeoFirst);
    connect(listSockets.at(1), &QIODevice::readyRead, this, &Client::readGeoSecond);
    connect(listSockets.at(2), &QIODevice::readyRead, this, &Client::readGeoThird);
    for (int i = 0; i < 3; i++) {
        connect(listSockets.at(i), QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &Client::displayError);
    }
}

void Client::connectToDefiniteHost(int id, const QString &ip, int port)
{
    qDebug() << " id = " << id << " ip = " << ip << " port = " << port;
    listSockets.at(id)->abort();
    listSockets.at(id)->connectToHost(ip, port);
    //listSockets.at(id)->connectToHost(listAdresses.at(id), listPorts.at(id));
}

void Client::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << " The host was not found. Please check the host name and port settings. ";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << "The connection was refused by the peer. "
                    "Make sure the fortune server is running, "
                    "and check that the host name and port "
                    "settings are correct.";
        break;
    default:
        qDebug() << " The following error occurred: ";// << tcpSocket->errorString();
    }
}

void Client::readGeoFirst() { readGeo(ID::FIRST); }

void Client::readGeoSecond() {readGeo(ID::SECOND); }

void Client::readGeoThird() { readGeo(ID::THIRD); }

void Client::readGeo(Client::ID id)
{
    in.setDevice(listSockets.at(id));
    in.setVersion(QDataStream::Qt_4_0);
    in.startTransaction();
    QString lat, tab, lon;
    in >> lat >>tab >> lon;
    qDebug() << " lat = " << lat << " lon = " << lon;
    if (!in.commitTransaction())
        return;

    QVariantList pointCoords;
    pointCoords.append(lat);
    pointCoords.append(lon);
    geoUpdated(pointCoords, id);
}







