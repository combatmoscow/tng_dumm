#include "serverapi.h"

Server_Api::Server_Api(QObject *parent) : QObject(parent)
{
    server = new Server();
    connect(server, &Server:: sigSendData, this, &Server_Api:: sltSendData);
    connect(server->tcpServer, &QTcpServer::newConnection, this, &Server_Api::sltConnectedToServer);
    connect(server, &Server::sigDisconnected, this, &Server_Api::sltDisconnectedFromServer);
}


bool Server_Api::startClicked()
{
    if (!server->tcpServer->listen(QHostAddress::Any, 6547))
    {
        return 0; // Error
    }
    else
    {
        connect(server->tcpServer, &QTcpServer::newConnection, server, &Server::newConnection);
        return 1;
    }
}

bool Server_Api::stopClicked()
{
    if(server->tcpServer->isListening())
    {
        disconnect(server->tcpServer, &QTcpServer::newConnection, server, &Server::newConnection);

        QList<QTcpSocket *> clients = server->getClients();
        for(int i = 0; i < clients.count(); i++)
        {

            server->sendToClient(clients.at(i), "0");
        }

        server->tcpServer->close();
        return 1; // Server closed
    }
    else
    {
        return 0; //err
    }
}



void Server_Api::sltConnectedToServer()
{
    emit sigConnected();
}

void Server_Api::sltDisconnectedFromServer()
{
    emit sigDisconnected();
}

 void Server_Api:: sltSendData(QByteArray data)
 {
    emit sidData( data);

 }
